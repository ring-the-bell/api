<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function get_products_count($store_id) {
		return $this->db->get_where('products', array('store_id' => $store_id, 'status' => 0))->result();
	}

	public function get_orders_count($store_id) {
		return $this->db->get_where('orders', array('store_id' => $store_id, 'status' => 0))->result();
	}

}

/* End of file dashboard_model.php */
/* Location: ./application/models/dashboard_model.php */