<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location_model extends CI_Model {
	
	public function get_states_by_country() {
		$stmt = "select id as stateId, name as stateName from states where country_id = 101";
		return $this->db->query($stmt)->result();
	}

	public function get_cities_by_state($state_id) {
		$stmt = "select id as cityId, name as cityName from cities where state_id = '" . $state_id . "'";
		return $this->db->query($stmt)->result();
	}

}

/* End of file Location_model.php */
/* Location: ./application/models/Location_model.php */