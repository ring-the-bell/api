<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	public function new_record($attributes) {		
		$this->db->insert('categories', $attributes);
		return $this->db->insert_id();
	}

	public function get_all() {
		$this->db->select('id as categoryId, name as categoryName, thumbnail as image, description');
		return $this->db->get('categories')->result();
	}

	public function check_record($store_id, $product_id, $name) {
		$stmt = "select name from categories where status = 0 and name = '" . $name . "'";
		return $this->db->query($stmt)->result();
	}

	public function update_record($category_id, $attributes) {
        $this->db->update('categories', $attributes, array('id' => $category_id, 'status' => 0));
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function delete_record($store_id, $product_id) {
		return $this->db->get_where('categories', array('id' => $product_id, 'store_id' => $store_id, 'status' => 0))->result();
	}

}

/* End of file category_model.php */
/* Location: ./application/models/category_model.php */