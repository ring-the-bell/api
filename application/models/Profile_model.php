<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model {

	public function get_store_info($store_id) {
		$this->db->select('name as storeName, logo as imageUrl, created_at as registeredDate, end_date as subscriptionDate');
		$this->db->where(array('id' => $store_id, 'status' => 0));
		return $this->db->get('stores')->row();
	}

	public function get_store_details($store_id) {
		$stmt = "select users.address, users.email, users.contact_no as storeNumber, stores.id as storeId, stores.name as storeName, stores.logo as imageUrl, stores.delivery as deliveryAvailable, stores.min_amount as minmumPurchase, stores.charges as deliveryCharges from stores join users on users.id = stores.user_id where users.status = stores.status and stores.id = " . $store_id;
		return $this->db->query($stmt)->row();
	}

	public function get_user_details($user_id) {
		return $this->db->get_where('users', array('id' => $user_id, 'status' => 0))->row();
	}

	public function update_user($attributes, $condition) {
		$this->db->update('users', $attributes, $condition);
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function update_store($attributes, $condition) {
		$this->db->update('stores', $attributes, $condition);
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function get_user_info($user_id) {
		$stmt = "select s.name as state, c.name as city, u.name as fullName, u.contact_no as mobileNumber, u.address, u.email from users as u left join states as s on u.state=s.id left join cities as c on u.city = c.id where c.state_id = s.id and u.status = 0 and u.id = " . $user_id;
		return $this->db->query($stmt)->row();
	}

}

/* End of file Profile_model.php */
/* Location: ./application/models/Profile_model.php */