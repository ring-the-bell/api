<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_Model {

	public function get_countries() {
		$this->db->select('id as countryId, name as countryName');
		return $this->db->get('countries')->result();
	}

	public function get_states_by_country($country_id) {
		$stmt = "select id as stateId, name as stateName from states where country_id = '" . $country_id . "'";
		return $this->db->query($stmt)->result();
	}

	public function get_cities_by_state($state_id) {
		$stmt = "select id as cityId, name as cityName from cities where state_id = '" . $state_id . "'";
		return $this->db->query($stmt)->result();
	}		

}

/* End of file Service_model.php */
/* Location: ./application/models/Service_model.php */