<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function new_user($attributes) {
		$this->db->insert('users', $attributes);
		return $this->db->insert_id();
	}

	public function get_user_details($user_id) {
		$this->db->select('select id as userId, name as fullName, contact_no as mobileNumber, address, email, state as stateId, city as cityId');
		$this->db->where('id', $user_id);
		$this->db->where('status', 0);
		return $this->db->get('users')->row();
	}
}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */