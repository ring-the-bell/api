<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Util_model extends CI_Model {

	public function get_all_categories() {
		$this->db->select('id as categoryId, name as categoryName, thumbnail as image, description');
		return $this->db->get('categories')->result();
	}

	public function get_products_count($store_id) {
		return $this->db->get_where('products', array('store_id' => $store_id, 'status' => 0))->result();
	}

	public function get_orders_count($store_id) {
		return $this->db->get_where('orders', array('store_id' => $store_id, 'status' => 0))->result();
	}

	public function get_store_shipping_details($store_id) {
		$stmt = "select users.contact_no as storeNumber, stores.name as storeName, stores.delivery as deliveryAvailable, stores.min_amount as minmumPurchase, stores.charges as deliveryCharges from stores join users on users.id = stores.user_id where users.status = stores.status and stores.id = " . $store_id;
		return $this->db->query($stmt)->row();
	}

	// public function get_all_stores() {
	// 	$stmt = "select users.address, users.contact_no as storeNumber, stores.id as storeId, stores.name as storeName, stores.logo as imageUrl, stores.delivery as deliveryAvailable, stores.min_amount as minmumPurchase, stores.charges as deliveryCharges from stores join users on users.id = stores.user_id where users.status = stores.status";
	// 	return $this->db->query($stmt)->result();
	// }

	public function get_all_stores() {
		$stmt = "select users.address as storeAddress,stores.id as storeId, stores.name as storeName, stores.logo as imageUrl from stores join users on users.id = stores.user_id where stores.status = 0";
		return $this->db->query($stmt)->result();
	}

	public function get_store_details_by_id($store_id) {
		$stmt = "select users.address, users.contact_no as storeNumber, stores.id as storeId, stores.name as storeName, stores.logo as imageUrl, stores.delivery as deliveryAvailable, stores.min_amount as minmumPurchase, stores.charges as deliveryCharges from stores join users on users.id = stores.user_id where users.status = stores.status and stores.id = " . $store_id;
		return $this->db->query($stmt)->row();
	}

	public function get_store_details_by_name($req_obj) {
		$stmt = "select stores.id as storeId, stores.name as storeName, stores.logo as imageUrl, stores.delivery as deliveryAvailable, stores.min_amount as minmumPurchase, stores.charges as deliveryCharges from stores join users on users.id = stores.user_id where stores.status = users.status and stores.name like '%". $req_obj->storeName ."%'";
		if (($req_obj->stateId)) {
			$stmt = $stmt . " and users.state = " . $req_obj->stateId;
		}

		if (($req_obj->cityId)) {
			$stmt = $stmt . " and users.city = " . $req_obj->cityId;
		}

		return $this->db->query($stmt)->result();
	}

	public function get_all_stores_by_location($state_id, $city_id) {
		$stmt = "select users.address, stores.id as storeId, stores.name as storeName, stores.logo as imageUrl, stores.delivery as deliveryAvailable, stores.min_amount as minmumPurchase, stores.charges as deliveryCharges from stores join users on users.id = stores.user_id where users.status = stores.status and users.state = " . $state_id . " and users.city = ". $city_id;
		return $this->db->query($stmt)->result();
	}

	public function store_produts_count($store_id) {
		$stmt = "select count(*) from products where status = 0 and store_id = " . $store_id;
		return $this->db->query($stmt)->row();
	}

	public function store_orders_count($store_id) {
		$stmt = "select count(*) from invoices where status = 0 and store_id = " . $store_id;
		return $this->db->query($stmt)->row();
	}

	public function get_all_products_by_store_id($store_id) {
		$stmt = "select id as productId, category_id as productCategoryId, name as productName, thumbnail_path as productImage, price as productPrice, offer as productOffer, offer_price as productOfferPrice, pack_weight as productPackWeight, type as productPackType, stock_weight as productPackStock, status as productStatus from products where status = 0 and store_id =".$store_id;
		return $this->db->query($stmt)->result();
	}

	public function get_all_products($store_id) {
		$stmt = "select id as productId, category_id as productCategoryId, name as productName, thumbnail_path as productImage, price as productPrice, offer as productOffer, offer_price as productOfferPrice, pack_weight as productPackWeight, type as productPackType, stock_weight as productPackStock, status as productStatus from products where store_id = ".$store_id;
		return $this->db->query($stmt)->result();
	}

	public function is_product_available($store_id, $product_id) {
		return $this->db->get_where('products', array('id' => $product_id, 'store_id' => $store_id, 'status' => 0))->result();
	}	

	public function delete_product($store_id, $product_id, $attributes) {
        $this->db->update('products', $attributes, array('id' => $product_id, 'store_id' => $store_id));
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function get_all_products_by_name($store_id, $choice) {
		$this->db->select('id as productId, category_id as productCategoryId, name as productName, thumbnail_path as productImage, price as productPrice, offer as productOffer, offer_price as productOfferPrice, pack_weight as productPackWeight, type as productPackType, stock_weight as productPackStock, status as productStatus from products');
		$this->db->from('products');
		$this->db->like('name', $choice);
		$this->db->where(array('store_id' => $store_id, 'status' => 0));
		return $this->db->get()->result();
	}

	public function check_product_status($product_id, $store_id) {
		return $this->db->get_where('orders', array('product_id' => $product_id, 'store_id' => $store_id, 'status' => 0))->result();
	}

	public function search_product($store_id, $product_name) {
		$stmt = "select id as productId, category_id as productCategoryId, name as productName, thumbnail_path as productImage, price as productPrice, offer as productOffer, offer_price as productOfferPrice, pack_weight as productPackWeight, type as productPackType, stock_weight as productPackStock, status as productStatus from products where store_id = ". $store_id . " and name like '%". $product_name ."%'";
		return $this->db->query($stmt)->result();
	}

	public function get_deployment_detaills() {
		$last = $this->db->select('version_no as versionNumber, description as dispalyText, deployeed_date as releaseDate')
				->order_by('id',"desc")
				->limit(1)
				->get('deployment')
				->row();
		return $last;
	}

	public function get_deployment_details() {
		$last = $this->db->select('id as subscriptionId, plan_name as subscriptionName, plan_price as subscriptionPrice, support_access as isSupportAvailable, email_access as isEmailAvailable, sms_access as isSmsAvailable, plan_clr as subscriptionColor')
				->where('status', 0)
				->get('subscription_plans')
				->result();
		return $last;
	}

}

/* End of file util_model.php */
/* Location: ./application/models/util_model.php */