<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function get_cart_items($guest_token) {
		$stmt = "select g.store_id as storeId, g.product_id as productId, p.name as productName, p.thumbnail_path as imageUrl, p.price as productPrice, p.offer_price as productOfferPrice, p.type as productPackType, g.quantity as productQuantity, p.pack_weight as productPackWeight from products as p left join guest_orders as g on g.product_id=p.id where g.store_id = p.store_id and g.status = 0 and g.guest_token = " . $guest_token;
		return $this->db->query($stmt)->result();
	}

	public function get_product_details($store_id, $product_id) {
		return $this->db->get_where('products', array('id' => $product_id, 'store_id' => $store_id, 'status' => 0))->row();
	}
	
	public function add_to_cart($attributes) {
		$this->db->insert('guest_orders', $attributes);
		return $this->db->insert_id();
	}

	public function verify_cart_item($attributes) {
		return $this->db->get_where('guest_orders', $attributes)->result();
	}

	public function get_guest_cart_items($attributes) {
		return $this->db->get_where('guest_orders', $attributes)->result();
	}

	public function update_cart($guest_token, $store_id, $product_id, $attributes) {
		$this->db->update('guest_orders', $attributes, array('guest_token' => $guest_token, 'store_id' => $store_id, 'product_id' => $product_id, 'status' => 0));
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function remove_guest_cart($attributes) {
		$this->db->update('guest_orders', array('status' => 2), $attributes);
        $this->db->trans_complete();
        return $this->db->trans_status();
		// $this->db->delete('guest_orders', $attributes);
		// return $this->db->affected_rows();
	}

	public function verify_user_cart_item($attributes) {
		return $this->db->get_where('orders', $attributes)->row();
	}

	public function get_user_cart_items($user_id, $action) {
		$prefix = "select ";
		$stmt = "o.store_id as storeId, o.product_id as productId, p.name as productName, p.thumbnail_path as imageUrl, p.price as productPrice, p.offer_price as productOfferPrice, p.type as productPackType, o.quantity as productQuantity, p.pack_weight as productPackWeight from products as p left join orders as o on o.product_id = p.id where o.store_id = p.store_id and o.status = 0 and o.user_id = " . $user_id;

		if (isset($action)) {
			$stmt = "o.id as order_id, " . $stmt;
		}

		return $this->db->query($prefix.$stmt)->result();
	}

	public function add_user_cart($attributes, $type) {
		if ($type == "batch") {
			$this->db->insert_batch('orders', $attributes);
			return $this->db->affected_rows();			
		} else {
			$this->db->insert('orders', $attributes);
			return $this->db->affected_rows();
		}
	}

	public function update_user_cart($attributes, $condition) {
		$this->db->update('orders', $attributes, $condition);
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function remove_user_cart($attributes) {
		$this->db->update('orders', array('status' => 2), $attributes);
        $this->db->trans_complete();
        return $this->db->trans_status();
		// $this->db->delete('orders', $attributes);
		// return $this->db->affected_rows();
	}

	public function get_invoices_count() {
		$query = $this->db->query('select * from invoices');
		return $query->num_rows();
	}

	public function new_invoice($attributes) {
		$this->db->insert('invoices', $attributes);
		return $this->db->insert_id();
	}

	public function invoice_order_map($attributes) {
		$this->db->insert('invoice_order_rel', $attributes);
		return $this->db->insert_id();
	}

	public function get_latest_invoice_id() {
		$last = $this->db->order_by('id',"desc")
		->limit(1)
		->get('invoices')
		->row();
		return $last;
	}

	public function get_user_invoice($user_id) {
		$stmt = "select i.id as invoiceId, i.order_invoice_id as orderNumber, i.name as customerName, i.contact_no as customerNumber, i.address as customerAddress, i.delivery_type as shippingType, i.charges as shippingCharges, i.status as orderStatus, i.created_at as orderedDate, s.name as storeName, u.contact_no as storeNumber, u.address as storeAddress from users as u left join stores as s on s.user_id = u.id left join invoices as i on s.id = i.store_id where i.store_id = s.id and i.user_id = " . $user_id;
		return $this->db->query($stmt)->result();
	}

	public function get_invoice_order_details($invoice_id, $user_id) {
		$stmt = "select p.name as productname, o.offer_price as productPrice, o.quantity as productQuantity from orders as o left join invoice_order_rel as io on io.order_id = o.id left join invoices as i on i.id = io.invoice_id left join products as p on p.id = o.product_id left join stores as s on s.id = i.store_id left join users as u on u.id = i.user_id where i.id = " . $invoice_id ." and u.id = " . $user_id;
		return $this->db->query($stmt)->result();
	}

	public function get_store_invoice($store_id) {
		$stmt = "select i.id as invoiceId, i.order_invoice_id as orderNumber, i.name as customerName, i.contact_no as customerNumber, i.address as customerAddress, i.delivery_type as shippingType, i.charges as shippingCharges, i.status as orderStatus, i.created_at as orderedDate, u.contact_no as storeNumber, u.address as storeAddress from users as u left join stores as s on s.user_id=u.id left join invoices as i on s.id = i.store_id where i.store_id = " . $store_id;
		return $this->db->query($stmt)->result();
	}

	public function get_store_invoice_order_details($invoice_id, $store_id) {
		$stmt = "select p.name as productname, o.offer_price as productPrice, o.quantity as productQuantity from orders as o left join invoice_order_rel as io on io.order_id = o.id left join invoices as i on i.id=io.invoice_id left join products as p on p.id=o.product_id left join stores as s on s.id=i.store_id left join users as u on u.id = i.user_id where i.id = " . $invoice_id ." and s.id = " . $store_id;
		return $this->db->query($stmt)->result();
	}

	public function update_invoice($attributes, $condition) {
		$this->db->update('invoices', $attributes, $condition);
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

}

/* End of file Order_model.php */
/* Location: ./application/models/Order_model.php */