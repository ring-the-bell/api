<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_model extends CI_Model {

	public function new_store($attributes) {
		$this->db->insert('stores', $attributes);
		return $this->db->insert_id();
	}

	public function verify_record($col_name, $col_val) {
		return $this->db->get_where('stores', array($col_name => $col_val, 'status' => 0))->num_rows();
	}

}

/* End of file store_model.php */
/* Location: ./application/models/store_model.php */