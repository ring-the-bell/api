<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {
	public function new_record($attributes) {		
		$this->db->insert('products', $attributes);
		return $this->db->insert_id();
	}

	public function get_all_by_store_id($store_id) {
		$stmt = "select id as productId, category_id as productCategoryId, name as productName, thumbnail_path as productImage, price as productPrice, offer as productOffer, pack_weight as productPackWeight, stock_weight as productPackStock, status as productStatus from products where status = 0 and store_id =".$store_id;
		return $this->db->query($stmt)->result();
	}

	public function check_record_on_add($store_id, $weight, $name) {
		return $this->db->get_where('products', array('status' => 0, 'name' => $name, 'pack_weight' => $weight, 'store_id' => $store_id))->result();
		// $stmt = "select name from products where status = 0 and name = '" . $name . "' and pack_weight = '" . $weight . "' and store_id = " . $store_id ;
		// return $this->db->query($stmt)->result();
	}

	public function check_record_on_update($store_id, $product_id, $name, $weight) {
		$stmt = "select name from products where status = 0 and name = '" . $name . "' and pack_weight = '" . $weight . "' and store_id = " . $store_id . " and id != " . $product_id;
		return $this->db->query($stmt)->result();
	}

	public function update_record($store_id, $product_id, $attributes) {
        $this->db->update('products', $attributes, array('id' => $product_id, 'store_id' => $store_id));
        $this->db->trans_complete();
        return $this->db->trans_status();
	}

	public function delete_record($store_id, $product_id) {
		return $this->db->get_where('products', array('id' => $product_id, 'store_id' => $store_id, 'status' => 0))->result();
	}
}

/* End of file product_model.php */
/* Location: ./application/models/product_model.php */