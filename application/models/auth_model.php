<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function new_user($attributes) {
		$this->db->insert('users', $attributes);		
		return $this->db->insert_id();
	}

	public function verify_user($val) {		
		$stmt = "select id, role_id, name as fullName, contact_no as mobileNumber, address, email, password_hash, state as stateId, city as cityId from users where status = 0 and contact_no = '". $val ."' or name = '". $val . "'";
		return $this->db->query($stmt)->row();
	}

}

/* End of file auth_model.php */
/* Location: ./application/models/auth_model.php */