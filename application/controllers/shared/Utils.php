<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Utils extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('util_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function store_name_exist($store_name) {
		if (isset($store_name) && $store_name) {
			$store_exist = _verify_record('stores', 'name', $store_name);

			if ($store_exist) {
				return _response_obj(400, ('store name' . RECORD_EXIST), null);
			} else {
				return _response_obj(200, null, null);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function user_name_exist($user_name) {
		if (isset($user_name) && $user_name) {
			$name_exist = _verify_record('users', 'name', $user_name);

			if ($name_exist) {
				return _response_obj(400, ('name' . RECORD_EXIST), null);
			} else {
				return _response_obj(200, null, null);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function contact_number_exist($contact_number) {
		if (isset($contact_number) && $contact_number) {
			$number_exist = _verify_record('users', 'contact_no', $contact_number);

			if ($number_exist) {
				return _response_obj(400, ('mobile Number' . RECORD_EXIST), null);
			} else {
				return _response_obj(200, null, null);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function email_exist($email) {
		if (isset($email) && $email) {
			$email_exist = _verify_record('users', 'email', $email);

			if ($email_exist) {
				return _response_obj(400, ('email' . RECORD_EXIST), null);
			} else {
				return _response_obj(200, null, null);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function get_all_stores() {
		// $post_data = file_get_contents("php://input");
		// $req_obj = json_decode($post_data);
		// if (!isset($req_obj)) {
		// 	return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		// }

		// if (!isset($req_obj->lat)) {
		// 	return _response_obj(400, ('latitude' . KEY_FAILURE), null);
		// }

		// if (!isset($req_obj->lang)) {
		// 	return _response_obj(400, ('langitude' . KEY_FAILURE), null);
		// }

		// $coef = 5000 * 0.0000089;
		// $attributes = array(
		// 	'min_lat' => $req_obj->lat,
		// 	'max_lat' => $req_obj->lat + $coef,
		// 	'min_lang' => $req_obj->lang,
		// 	'max_lang' => $req_obj->lang + $coef / cos($req_obj->lat * 0.018)
		// );

		$result = $this->util_model->get_all_stores();
		return _response_obj(200, null, $result);
	}

	public function get_store_details_by_id($store_id) {
		if (isset($store_id) && !empty($store_id)) {
			$result = $this->util_model->get_store_details_by_id($store_id);
			return _response_obj(200, null, $result);
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function get_store_details() {
		$post_data = file_get_contents("php://input");
		$req_obj = json_decode($post_data);
		if (isset($req_obj)) {
			$result = $this->util_model->get_store_details_by_name($req_obj);
			return _response_obj(200, null, $result);
		} else {
			return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		}
	}

	public function get_store_shipping_details($store_id) {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_all_by_id("users", "id", $user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
					if (isset($role) && $role && $role->name == 'shopper') {
						$shipping = $this->util_model->get_store_shipping_details($store_id);
						if (isset($shipping) && $shipping) {
							return _response_obj(200, null, $shipping);
						} else {
							return _response_obj(400, STORE_NOT_FOUND, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function get_all_products($store_id) {
		if (isset($store_id)) {
			if ($store_id > 0) {
				$result = $this->util_model->get_all_products_by_store_id($store_id);
				return _response_obj(200, null, $result);
			} else {
				return _response_obj(400, INVALID_PARAM, $result);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, $result);
		}	
	}

	public function get_all_products_store() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						$result = $this->util_model->get_all_products($acc_details->store_id);
						return _response_obj(200, null, $result);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}	
	}

	public function get_all_products_by_name($store_id, $choice) {
		if (!isset($store_id)) {
			return _response_obj(400, ('storeId' . KEY_FAILURE), null);
		} else {
			if (empty($store_id)) {
				return _response_obj(400, ('storeId' . EMPTY_VALUE), null);
			}
		}

		if (!isset($choice)) {
			return _response_obj(400, ('choice' . KEY_FAILURE), null);
		} else {
			if (empty($choice)) {
				return _response_obj(400, ('choice' . EMPTY_VALUE), null);
			}
		}

		$result = $this->util_model->get_all_products_by_name($store_id, $choice);
		return _response_obj(200, null, $result);
	}

	public function get_all_products_by_store() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						$this->get_all_products($acc_details->store_id);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function check_product_availability($product_id) {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						
						if (!isset($product_id)) {
							return _response_obj(400, ('productId' . KEY_FAILURE), null);
						} else {
							if (empty($product_id)) {
								return _response_obj(400, ('productId' . EMPTY_VALUE), null);
							}
						}

						$result = $this->util_model->check_product_status($product_id, $acc_details->store_id);
						if ($result && count($result) > 0) {
							return _response_obj(400, ADDED_IN_USER_CART, null);
						} else {
							return _response_obj(200, null, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function statistics($type) {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						if (isset($type)) {
							if ($type == 'products') {
								$this->_get_store_products_count($acc_details->store_id);
							} else if ($type == 'orders') {
								$this->_get_store_orders_count($acc_details->store_id);
							} else if ($type == 'pendingOrders') {

							} else {
								return _response_obj(400, INVALID_PARAM, null);
							}
						} else {
							return _response_obj(400, QUERY_PARAM_MISSING, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function _get_store_products_count($store_id) {
		$result = $this->util_model->store_produts_count($store_id);
		return _response_obj(200, null, $result);
	}

	public function _get_store_orders_count($store_id) {
		$result = $this->util_model->store_orders_count($store_id);
		return _response_obj(200, null, $result);
	}

	public function get_categories() {
		$result = $this->util_model->get_all_categories();
		return _response_obj(200, null, $result);
	}

	public function get_all_stores_by_location() {
		$post_data = file_get_contents("php://input");
		$req_obj = json_decode($post_data);

		if(isset($req_obj)) {
			if (!isset($req_obj->stateId)) {
				return _response_obj(400, ('state' . KEY_FAILURE), null);
			}

			if (!isset($req_obj->cityId)) {
				return _response_obj(400, ('city' . KEY_FAILURE), null);
			}

			$result = $this->util_model->get_all_stores_by_location($req_obj->stateId, $req_obj->cityId);
			return _response_obj(200, null, $result);

		} else {
			return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		}
	}

	public function search_product($product_name) {
		if ($product_name) {
			if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
				if (_verify_session_token($this->input->request_headers())) {
					$user_id = _decrypt_session_token($this->input->request_headers());
					$acc_details = _get_store__related_details($user_id);
					if ( isset($acc_details) && $acc_details ) {
						$role = _get_role_details('name', 'id', $acc_details->role_id);
						if (isset($role) && $role && $role->name == 'retailer') {
							$result = $this->util_model->search_product($acc_details->store_id, $product_name);
							return _response_obj(200, null, $result);
						} else {
							return _response_obj(403, UN_AUTHORIZED, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(401, SESSION_FAILURE, null);					
				}
			} else {
				return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function get_deployment_detaills() {
		$result = $this->util_model->get_deployment_detaills();
		return _response_obj(200, null, $result);
	}

	public function new_suggestion() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && ($role->name == 'retailer' || $role->name == 'shopper')) {
						// $result = $this->util_model->search_product($acc_details->store_id, $product_name);
						return _response_obj(200, null, $result);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function get_subscription_details() {
		$result = $this->util_model->get_deployment_details();
		return _response_obj(200, null, $result);
	}

	public function check_dates() {
		$present_date = date("Y-m-d H:i:s");
		$recrd_date = "2020-06-14 17:14:03";
		
		if ($present_date > $recrd_date) 
		    echo $present_date . " is latest than "
		            . $recrd_date; 
		else
		    echo $present_date . " is older than " 
		            . $recrd_date; 
	}

}

/* End of file Utils.php */
/* Location: ./application/controllers/shared/Utils.php */