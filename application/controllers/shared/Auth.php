<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function user_sign_in() {
		$post_data = file_get_contents("php://input");
		$req_obj = json_decode($post_data);
		if ($req_obj) {
			if (!isset($req_obj->username)) {
				return _response_obj(400, ('username' . KEY_FAILURE), null);
			} else {
				if (empty($req_obj->username)) {
					return _response_obj(400, ('username' . EMPTY_VALUE), null);
				}
			}

			if (!isset($req_obj->password)) {
				return _response_obj(400, ('password' . KEY_FAILURE), null);
			} else {
				if (empty($req_obj->password)) {
					return _response_obj(400, ('password' . EMPTY_VALUE), null);
				}
			}


			$user_data = $this->auth_model->verify_user($req_obj->username);

			if (isset($user_data)) {
				if (password_verify($req_obj->password, $user_data->password_hash)) {
					$tokenData = array();
					$tokenData['id'] = $user_data->id; 
					$tokenData['timestamp'] = now();
					$output['token'] = AUTHORIZATION::generateToken($tokenData);
					$token = _session_key_enc_dec_ryption($output['token'], 'e');
					if (isset($user_data->role_id)) {
						$user_data->userType = (($user_data->role_id == 2) ? 'retailer' : 'shopper');
						$user_data->userId = $user_data->id;
						unset($user_data->id);
						unset($user_data->password_hash);
						unset($user_data->role_id);
						return _response_obj(200, LOGIN_SUCCESS, array('access_token' =>  _session_key_enc_dec_ryption($output['token'], 'e'), 'data' => $user_data));
					} else {
						return _response_obj(400, LOGIN_FAILURE, null);
					}
				} else {
					return _response_obj(400, LOGIN_FAILURE, null);
				}
			} else {
				return _response_obj(400, LOGIN_FAILURE, null);
			}			
		} else {
			return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		}
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/shared/Auth.php */