<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function get_store_info() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						$result = $this->profile_model->get_store_info($acc_details->store_id);
						return _response_obj(200, null, $result);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function get_store_details() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						$result = $this->profile_model->get_store_details($acc_details->store_id);
						return _response_obj(200, null, $result);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function update_password() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_all_by_id("users", "id", $user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
					if (isset($role) && $role && ($role->name == 'retailer' || $role->name == 'shopper')) {
						$post_data = file_get_contents("php://input");
						$req_obj = json_decode($post_data);
						if (isset($req_obj) && $req_obj) {
							if (!isset($req_obj->oldPassword)) {
								return _response_obj(400, ('oldPassword' . KEY_FAILURE), null);
							} else {
								if (empty($req_obj->oldPassword)) {
									return _response_obj(400, ('oldPassword' . EMPTY_VALUE), null);
								}
							}
							if (!isset($req_obj->newPassword)) {
								return _response_obj(400, ('newPassword' . KEY_FAILURE), null);
							} else {
								if (empty($req_obj->newPassword)) {
									return _response_obj(400, ('newPassword' . EMPTY_VALUE), null);
								}
							}

							$user_data = $this->profile_model->get_user_details($user_id);

							if (isset($user_data)) {
								if (password_verify($req_obj->oldPassword, $user_data->password_hash)) {
									if ($req_obj->oldPassword == $req_obj->newPassword) {
										return _response_obj(400, PASSWORDS_DIFFERENT_FAILURE, null);
									} else {
										$obj = array('password_hash' => password_hash($req_obj->newPassword, PASSWORD_DEFAULT));
										$condition = array('id' => $user_id, 'status' => 0);
										$result = $this->profile_model->update_user($obj, $condition);
										if (isset($result) && $result) {
											return _response_obj(200, RECORD_UPDATE_SUCCESS, null);
										} else {
											return _response_obj(400, SOMETHING_WENT_WORNG, null);
										}
									}
								} else {
									return _response_obj(400, OLD_PASSWORD_FAILURE, null);
								}
							} else {
								return _response_obj(400, RECORD_NOT_FOUND, null);
							}
						} else {
							return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function update_store() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && ($role->name == 'retailer')) {
						$req_obj = (object) $_POST;
						if (isset($req_obj) && $req_obj) {
							if (!isset($req_obj->storeName)) {
								return _response_obj(400, ('storeName' . KEY_FAILURE), null);
							} else {
								if (empty($req_obj->storeName)) {
									return _response_obj(400, ('storeName' . EMPTY_VALUE), null);
								}
							}

							if (!isset($req_obj->mobileNumber)) {
								return _response_obj(400, ('mobileNumber' . KEY_FAILURE), null);
							} else {
								if (empty($req_obj->mobileNumber)) {
									return _response_obj(400, ('mobileNumber' . EMPTY_VALUE), null);
								}
							}

							if (!isset($req_obj->address)) {
								return _response_obj(400, ('address' . KEY_FAILURE), null);
							} else {
								if (empty($req_obj->address)) {
									return _response_obj(400, ('address' . EMPTY_VALUE), null);
								}
							}

							if (isset($req_obj->delivery)) {
								if (!isset($req_obj->amount)) {
									return _response_obj(400, ('amount' . KEY_FAILURE), null);
								}
								if (!isset($req_obj->charges)) {
									return _response_obj(400, ('charges' . KEY_FAILURE), null);
								}
							}

							$user_obj = array('contact_no' => $req_obj->mobileNumber, 'address' => $req_obj->address, 'email' => $req_obj->email);
							$user_condition = array('id' => $user_id);

							$user_data = $this->profile_model->update_user($user_obj, $user_condition);

							$store_obj = array(
								'name' => $req_obj->storeName,
								'delivery' => $req_obj->delivery == 'true' ? 0 : 1,
								'min_amount' => $req_obj->amount ? $req_obj->amount : '',
								'charges' => $req_obj->charges ? $req_obj->charges : '',
							);
							$store_condition = array('user_id' => $user_id, 'id' => $acc_details->store_id);

							if (isset($_FILES) && isset($_FILES['storeLogo'])) {
								// var_dump($_FILES['logo']);
								$upload_path = './uploads/stores';
								//file upload destination
								$config['upload_path'] = $upload_path;
								//allowed file types. * means all types
								$config['allowed_types'] = 'jpeg|jpg|png';
								//allowed max file size. 0 means unlimited file size
								$config['max_size'] = '0';
								//max file name size
								$config['max_filename'] = '255';
								//whether file name should be encrypted or not
								$config['encrypt_name'] = TRUE;

								$file_types = array("image/jpeg", "image/png", "image/jpg");

								if (!in_array($_FILES['storeLogo']['type'], $file_types)) {
									return _response_obj(400, INCORRECT_FILE_FORMAT, null);
								} else {
									$this->load->library('upload', $config);

									if (!file_exists($upload_path)) {
										mkdir($upload_path, 0777, true);
									}

									if($this->upload->do_upload('storeLogo')) {
										$image_data = (object) $this->upload->data();
										$img_path = explode("/home/c8ugqa8m8lxk/public_html/api/", $image_data->full_path);
										$store_obj['logo'] = ((isset($img_path[1]) && $image_data->full_path) ? ("http://ringthebell.in/api/" . $img_path[1]) : $image_data->full_path);
									} else {
										return _response_obj(500, FILE_UPLOAD_FAIL, null);
									}											
								}										
							}

							$this->profile_model->update_store($store_obj, $store_condition);
							return _response_obj(200, RECORD_UPDATE_SUCCESS, null);
						} else {
							return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function user_info() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_all_by_id("users", "id", $user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
					if (isset($role) && $role && ($role->name == 'shopper')) {
						$result = $this->profile_model->get_user_info($user_id);
						return _response_obj(200, null, $result);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/shared/Profile.php */