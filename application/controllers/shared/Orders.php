<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Orders extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function guest_cart_items($guest_token) {		
		if (isset($guest_token) && $guest_token) {
			$method_type = $_SERVER["REQUEST_METHOD"];
			if (isset($method_type)) {
				if ($method_type == "GET") {
					$result = $this->order_model->get_cart_items($guest_token);
					return _response_obj(200, null, $result);
				} else if ($method_type == "POST") {
					$post_data = file_get_contents("php://input");
					$req_obj = json_decode($post_data);
					$this->add_guest_cart_tems($guest_token, $req_obj);
				} else {
					return _response_obj(400, INCORECT_REQUEST_METHOD, null);
				}
			} else {
				return _response_obj(400, REQUEST_METHOD_FAILURE, null);
			}
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}
	}

	public function add_guest_cart_tems($guest_token, $req_obj) {
		if (isset($guest_token) && $guest_token && $req_obj) {
			if (!isset($req_obj->productId)) {
				return _response_obj(400, ('product id' . KEY_FAILURE), null);
			} else {
				if (empty($req_obj->productId)) {
					return _response_obj(400, ('product id' . EMPTY_VALUE), null);
				}
			}


			if (!isset($req_obj->storeId)) {
				return _response_obj(400, ('store id' . KEY_FAILURE), null);
			} else {
				if (empty($req_obj->storeId)) {
					return _response_obj(400, ('store id' . EMPTY_VALUE), null);
				}
			}


			if (!isset($req_obj->prodQty)) {
				return _response_obj(400, ('product quantity' . KEY_FAILURE), null);
			}

			$product_details = $this->order_model->get_product_details($req_obj->storeId, $req_obj->productId);

			if ($product_details && isset($product_details->price) && $product_details->price) {
				$result = $this->order_model->get_cart_items($guest_token);
				if ($result && count($result) > 0) {
					$data = array('guest_token' => $guest_token, 'store_id' => $req_obj->storeId, 'product_id' => $req_obj->productId, 'status' => 0);
					$records = $this->order_model->verify_cart_item($data);
					if ($records) {						
						if ($req_obj->prodQty <= 0) {
							$obj = array (
								"quantity" => $req_obj->prodQty,
								"status" => 1
							);
						} else {
							$obj = array (
								"quantity" => $req_obj->prodQty
							);
						}
						$updated = $this->order_model->update_cart($guest_token, $req_obj->storeId, $req_obj->productId, $obj);
						if ($updated) {
							return _response_obj(200, CART_UPDATE_SUCCESS, null);
						} else {
							return _response_obj(500, CART_UPDATE_FAIL, null);
						}
					} else {
						$obj = array(
							"guest_token" => $guest_token,
							"store_id" => $req_obj->storeId,
							"product_id" => $req_obj->productId,
							"quantity" => $req_obj->prodQty,
						);

						$this->_add_to_cart($obj);
					}
				} else {
					$obj = array(
						"guest_token" => $guest_token,
						"store_id" => $req_obj->storeId,
						"product_id" => $req_obj->productId,
						"quantity" => $req_obj->prodQty
					);

					$this->_add_to_cart($obj);
				}
			} else {
				return _response_obj(400, RECORD_NOT_FOUND, null);
			}
		} else {
			return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		}
	}

	public function remove_all($store_id) {
		if (!empty($store_id)) {
			if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
				if (_verify_session_token($this->input->request_headers())) {
					$user_id = _decrypt_session_token($this->input->request_headers());
					$acc_details = _get_all_by_id("users", "id", $user_id);
					if ( isset($acc_details) && $acc_details ) {
						$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
						if (isset($role) && $role && $role->name == 'shopper') {
							$data = array('user_id' => $user_id);
							$records = $this->order_model->remove_user_cart($data);
							if (isset($records) && $records) {
								// $deleted = $this->order_model->remove_guest_cart($data);
								// if ($deleted) {
								// 	return _response_obj(200, CART_REMOVED, null);
								// } else {
								// 	return _response_obj(500, SOMETHING_WENT_WORNG, null);
								// }
								return _response_obj(200, CART_REMOVED, null);
							} else {
								return _response_obj(200, null, null);
							}
						} else {
							return _response_obj(403, UN_AUTHORIZED, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(401, SESSION_FAILURE, null);				
				}
			} else {
				return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
			}			
		} else {
			return _response_obj(400, ('value' . EMPTY_VALUE), null);
		}
	}

	public function _remove_guest_cart_items($data) {
		$records = $this->order_model->verify_cart_item($data);
		if (isset($records) && $records) {
			$obj = array('guest_token' => $data['guest_token']);
			$deleted = $this->order_model->remove_guest_cart($obj);
			if ($deleted) {
				return _response_obj(200, CART_REMOVED, null);
			} else {
				return _response_obj(500, SOMETHING_WENT_WORNG, null);
			}
		} else {
			return _response_obj(200, null, null);
		}
	}

	public function _add_to_cart($obj) {
		$cart_id = $this->order_model->add_to_cart($obj);

		if ($cart_id && $cart_id > 0) {
			return _response_obj(200, ADD_CART_SUCCESS, null);
		} else {
			return _response_obj(500, CART_UPDATE_FAIL, null);
		}
	}

	public function user_cart_items() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_all_by_id("users", "id", $user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
					if (isset($role) && $role && $role->name == 'shopper') {
						$method_type = $_SERVER["REQUEST_METHOD"];

						if (isset($method_type)) {
							if ($method_type == "GET") {
								$result = $this->order_model->get_user_cart_items($user_id, null);
								return _response_obj(200, null, $result);
							} else if ($method_type == "POST") {
								$post_data = file_get_contents("php://input");
								$req_obj = json_decode($post_data);
								$product_details = $this->order_model->get_product_details($req_obj->storeId, $req_obj->productId);
								if (isset($product_details) && !empty($product_details) && $product_details) {
									if (!isset($req_obj->productId)) {
										return _response_obj(400, ('product id' . KEY_FAILURE), null);
									} else {
										if (empty($req_obj->productId)) {
											return _response_obj(400, ('product id' . EMPTY_VALUE), null);
										}
									}


									if (!isset($req_obj->storeId)) {
										return _response_obj(400, ('store id' . KEY_FAILURE), null);
									} else {
										if (empty($req_obj->storeId)) {
											return _response_obj(400, ('store id' . EMPTY_VALUE), null);
										}
									}


									if (!isset($req_obj->prodQty)) {
										return _response_obj(400, ('product quantity' . KEY_FAILURE), null);
									}
									
									$this->_user_add_to_cart($user_id, $req_obj);
								} else {
									return _response_obj(400, PRODUCT_REMOVED, null);
								}
							} else {
								return _response_obj(400, INCORECT_REQUEST_METHOD, null);
							}
						} else {
							return _response_obj(400, REQUEST_METHOD_FAILURE, null);
						}

					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function _user_add_to_cart($user_id, $req_obj) {
		$data = array('user_id' => $user_id, 'store_id' => $req_obj->storeId, 'product_id' => $req_obj->productId, 'status' => 0);
		$records = $this->order_model->verify_user_cart_item($data);

		if ($records) {
			if ($req_obj->prodQty <= 0) {
				$obj = array (
					"user_id" => $user_id,
					"store_id" => $req_obj->storeId,
					"product_id" => $req_obj->productId,
					"quantity" => $req_obj->prodQty,
					"status" => 1,
				);
			} else {
				$obj = array (
					"user_id" => $user_id,
					"store_id" => $req_obj->storeId,
					"product_id" => $req_obj->productId,
					"quantity" => $req_obj->prodQty,
					"status" => 0
				);
			}
			$result = $this->order_model->update_user_cart($obj, $data);
			if (isset($result) && !empty($result) && $result) {					
				return _response_obj(200, CART_UPDATE_SUCCESS, null);
			} else {
				return _response_obj(500, CART_UPDATE_FAIL, null);
			}
		} else {
			if ($req_obj->prodQty <= 0) {
				$obj = array (
					"user_id" => $user_id,
					"store_id" => $req_obj->storeId,
					"product_id" => $req_obj->productId,
					"quantity" => $req_obj->prodQty,
					"status" => 1,
				);
			} else {
				$obj = array (
					"user_id" => $user_id,
					"store_id" => $req_obj->storeId,
					"product_id" => $req_obj->productId,
					"quantity" => $req_obj->prodQty,
					"status" => 0
				);
			}

			$user_cart = $this->order_model->add_user_cart($obj, "single");

			if ($user_cart && $user_cart > 0) {					
				return _response_obj(200, ADD_CART_SUCCESS, null);
			} else {
				return _response_obj(500, CART_UPDATE_FAIL, null);
			}
		}
	}

	public function order_confirm() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_all_by_id("users", "id", $user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
					if (isset($role) && $role && $role->name == 'shopper') {
						$user_items = $this->order_model->get_user_cart_items($user_id, "confirm");
						if (isset($user_items) && $user_items && count($user_items) > 0) {
							$post_data = file_get_contents("php://input");
							$req_obj = json_decode($post_data);
							if (isset($req_obj) && $req_obj) {
								
								if (!isset($req_obj->fullName)) {
									return _response_obj(400, ('fullname' . KEY_FAILURE), null);
								} else {
									if (empty($req_obj->fullName)) {
										return _response_obj(400, ('fullname' . EMPTY_VALUE), null);
									}
								}

								if (!isset($req_obj->mobileNumber)) {
									return _response_obj(400, ('mobileNumber' . KEY_FAILURE), null);
								} else {
									if (empty($req_obj->mobileNumber)) {
										return _response_obj(400, ('mobileNumber' . EMPTY_VALUE), null);
									}
								}

								if (!isset($req_obj->address)) {
									return _response_obj(400, ('address' . KEY_FAILURE), null);
								} else {
									if (empty($req_obj->address)) {
										return _response_obj(400, ('address' . EMPTY_VALUE), null);
									}
								}

								if (!isset($req_obj->shipping)) {
									return _response_obj(400, ('shipping' . KEY_FAILURE), null);
								} else {
									if (empty($req_obj->shipping)) {
										return _response_obj(400, ('shipping' . EMPTY_VALUE), null);
									}
								}

								if (!isset($req_obj->storeId)) {
									return _response_obj(400, ('store id' . KEY_FAILURE), null);
								} else {
									if (empty($req_obj->storeId)) {
										return _response_obj(400, ('store id' . EMPTY_VALUE), null);
									}
								}

								$store_details = _get_all_by_id("stores", "id", $req_obj->storeId);

								if (isset($store_details) && $store_details) {
									$invoice_count = $this->order_model->get_invoices_count();

									if (isset($invoice_count) && $invoice_count && $invoice_count > 0) {
										$latest_invoice = $this->order_model->get_latest_invoice_id();
										if ($latest_invoice->id < 10) {
											$new_invoice_id = "RTBOD000" . ($latest_invoice->id + 1);
										} else if ($latest_invoice->id < 100) {
											$new_invoice_id = "RTBOD00" . ($latest_invoice->id + 1);
										} else if ($latest_invoice->id < 1000) {
											$new_invoice_id = "RTBOD0" . ($latest_invoice->id + 1);
										} else {
											$new_invoice_id = "RTBOD" . ($latest_invoice->id + 1);
										}
									} else {
										$new_invoice_id = "RTBOD" . "0001" ;
									}

									if (isset($new_invoice_id) && $new_invoice_id) {
										$obj = array(
											"order_invoice_id" => $new_invoice_id,
											"user_id" => $user_id,
											"store_id" => $store_details[0]->id,
											"name" => $req_obj->fullName,
											"contact_no" => $req_obj->mobileNumber,
											"address" => $req_obj->address,
											"delivery_type" => $req_obj->shipping,
											"charges" => ($req_obj->shipping == 'delivery' ? $store_details[0]->charges : 0),
											"status" => "waiting for approval"
										);
										$this->_generate_invoice($obj, $user_items, $user_id);
									} else {
										return _response_obj(500, SOMETHING_WENT_WORNG, null);
									}

								} else {
									return _response_obj(500, SOMETHING_WENT_WORNG, null);
								}
							} else {
								return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
							}
						} else {
							return _response_obj(400, EMPTY_CART, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function _generate_invoice($invoice_obj, $cart_items) {
		$last_invoice_id = $this->order_model->new_invoice($invoice_obj);
		foreach ($cart_items as $key => $value) {
			$product_details = $this->order_model->get_product_details($value->storeId, $value->productId);
			if (isset($product_details) && $product_details) {
				if (isset($last_invoice_id) && $last_invoice_id > 0) {
					$map_obj = array("invoice_id" => $last_invoice_id, "order_id" => $value->order_id);
					$success = $this->order_model->invoice_order_map($map_obj);
					if (isset($success) && $success > 0) {
						$obj = array(
							"price" => $product_details->price,
							"offer_price" => $product_details->offer_price,
							"weight" => $product_details->pack_weight,
							"type" => $product_details->type,
							"status" => 1
						);
						$condition = array("id" => $value->order_id);
						$this->order_model->update_user_cart($obj, $condition);
					}
				}
			}
		}

		return _response_obj(200, null, null);
	}

	public function get_user_invoice() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_all_by_id("users", "id", $user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
					if (isset($role) && $role && $role->name == 'shopper') {
						$orders = $this->order_model->get_user_invoice($user_id);
						return _response_obj(200, null, $orders);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function get_invoice_order_details($invoice_id) {
		if (isset($invoice_id) && $invoice_id > 0) {
			if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
				if (_verify_session_token($this->input->request_headers())) {
					$user_id = _decrypt_session_token($this->input->request_headers());
					$acc_details = _get_all_by_id("users", "id", $user_id);
					if ( isset($acc_details) && $acc_details ) {
						$role = _get_role_details('name', 'id', $acc_details[0]->role_id);
						if (isset($role) && $role && $role->name == 'shopper') {
							$orders = $this->order_model->get_invoice_order_details($invoice_id, $user_id);
							return _response_obj(200, null, $orders);
						} else {
							return _response_obj(403, UN_AUTHORIZED, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(401, SESSION_FAILURE, null);					
				}
			} else {
				return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
			}
		} else {
			return _response_obj(400, ('invoice id' . KEY_FAILURE), null);
		}
	}

	public function get_store_invoice() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						$result = $this->order_model->get_store_invoice($acc_details->store_id);
						return _response_obj(200, null, $result);
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function get_store_invoice_order_details($invoice_id) {
		if (isset($invoice_id) && $invoice_id > 0) {
			if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
				if (_verify_session_token($this->input->request_headers())) {
					$user_id = _decrypt_session_token($this->input->request_headers());
					$acc_details = _get_store__related_details($user_id);
					if ( isset($acc_details) && $acc_details ) {
						$role = _get_role_details('name', 'id', $acc_details->role_id);
						if (isset($role) && $role && $role->name == 'retailer') {
							$result = $this->order_model->get_store_invoice_order_details($invoice_id, $acc_details->store_id);
							return _response_obj(200, null, $result);
						} else {
							return _response_obj(403, UN_AUTHORIZED, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(401, SESSION_FAILURE, null);					
				}
			} else {
				return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
			}
		} else {
			return _response_obj(400, ('invoice id' . KEY_FAILURE), null);
		}
	}

	public function update_invoice() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$user_id = _decrypt_session_token($this->input->request_headers());
				$acc_details = _get_store__related_details($user_id);
				if ( isset($acc_details) && $acc_details ) {
					$role = _get_role_details('name', 'id', $acc_details->role_id);
					if (isset($role) && $role && $role->name == 'retailer') {
						$post_data = file_get_contents("php://input");
						$req_obj = json_decode($post_data);

						if (!isset($req_obj->invoiceId)) {
							return _response_obj(400, ('invoice id' . KEY_FAILURE), null);
						} else {
							if (empty($req_obj->invoiceId)) {
								return _response_obj(400, ('invoice id' . EMPTY_VALUE), null);
							}
						}

						if (!isset($req_obj->invoiceStatus)) {
							return _response_obj(400, ('invoice status' . KEY_FAILURE), null);
						} else {
							if (empty($req_obj->invoiceStatus)) {
								return _response_obj(400, ('invoice status' . EMPTY_VALUE), null);
							}
						}

						$obj = array('status' => $req_obj->invoiceStatus);
						$condition = array('id' => $req_obj->invoiceId, 'store_id' => $acc_details->store_id);

						$result = $this->order_model->update_invoice($obj, $condition);

						if (isset($result) && $result) {
							return _response_obj(200, RECORD_UPDATE_SUCCESS, null);
						} else {
							return _response_obj(500, SOMETHING_WENT_WORNG, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

}

/* End of file Orders.php */
/* Location: ./application/controllers/shared/Orders.php */