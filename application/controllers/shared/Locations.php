<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Locations extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('location_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function get_states_by_country() {
		$result = $this->location_model->get_states_by_country();
		return _response_obj(200, "", $result);
	}

	public function get_cities_by_state($state_id) {
		$result = $this->location_model->get_cities_by_state($state_id);
		return _response_obj(200, "", $result);
	}

}

/* End of file Locations.php */
/* Location: ./application/controllers/shared/Locations.php */