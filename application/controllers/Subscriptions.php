<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriptions extends CI_Controller {

	public function index()
	{
		$present_date = date("Y-m-d H:i:s");
		$rcrds = $this->db->select('id as store_id, user_id, end_date as subscriptionDate')
				->where('status', 0)
				->get('stores')
				->result();
		if (isset($rcrds) && $rcrds && count($rcrds) > 0) {
			foreach ($rcrds as $key => $value) {
				if ($value->subscriptionDate < $present_date) {
					$this->db->update('users', array('status' => 1), array('id' => $value->user_id));
					$this->db->update('stores', array('status' => 1), array('id' => $value->store_id));
				}
			}
		}
	}

}

/* End of file Subscriptions.php */
/* Location: ./application/controllers/shared/Subscriptions.php */