<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Categories extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('category_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function function_router($category_id) {
		$method_type = $_SERVER["REQUEST_METHOD"];
		if (isset($method_type)) {
			if ($method_type == "GET") {
				$this->_get_all();							
			} else {
				if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
					if (_verify_session_token($this->input->request_headers())) {
						$admin_id = _decrypt_session_token($this->input->request_headers());
						$role_id = _get_role_id('stores', $admin_id);
						 // $role_id = 1;
						// var_dump($role_id);
						if ($role_id && $role_id > 0) {
							if (_verify_role('retailer', $role_id)) {
								if (isset($admin_id) && $admin_id > 0) {
									$method_type = $_SERVER["REQUEST_METHOD"];
									if ($method_type == "POST") {
										$this->_new_category();
									} else if ($method_type == "PUT") {
										if ($category_id) {
											$this->_update_category($admin_id, $category_id);
										} else {
											return _response_obj(400, QUERY_PARAM_MISSING, null);
										}
									} else if ($method_type == "DELETE") {
										if ($product_id) {
											$this->_delete_product($admin_id, $product_id);
										} else {
											return _response_obj(400, QUERY_PARAM_MISSING, null);
										}
									} else {
										return _response_obj(400, INCORECT_REQUEST_METHOD, null);
									}
								} else {
									return _response_obj(401, SESSION_FAILURE, null);
								}
							} else {
								return _response_obj(403, UN_AUTHORIZED, null);
							}
						} else {
							return _response_obj(403, UN_AUTHORIZED, null);
						}
					} else {
						return _response_obj(401, SESSION_FAILURE, null);					
					}
				} else {
					return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
				}
			}
		} else {
			return _response_obj(400, REQUEST_METHOD_FAILURE, null);
		}

	}

	public function _get_all() {
		$list = $this->category_model->get_all();
		return _response_obj(200, null, $list);
	}

	public function _new_category() {
		if ($this->input->post() && count($this->input->post()) > 0) {
			if (isset($_FILES) && isset($_FILES['image'])) {
				$req_obj = (object) $this->input->post();
				if (isset($req_obj->categoryName)) {
					if (_verify_record('categories', 'name', $req_obj->categoryName)) {
						return _response_obj(400, (strtoupper($req_obj->categoryName) . RECORD_EXIST), null);
					}
				} else {
					return _response_obj(400, REQUEST_KEYS_FAILURE, null);
				}

				if (!isset($req_obj->description)) {
					$req_obj->description = '';
				}

				// var_dump($_FILES['logo']);
				$upload_path = './uploads/categories';
				//file upload destination
				$config['upload_path'] = $upload_path;
				//allowed file types. * means all types
				$config['allowed_types'] = '*';
				//allowed max file size. 0 means unlimited file size
				$config['max_size'] = '0';
				//max file name size
				$config['max_filename'] = '255';
				//whether file name should be encrypted or not
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config);

				if (!file_exists($upload_path)) {
					mkdir($upload_path, 0777, true);
				}
				
				if($this->upload->do_upload('image')) {
					$image_data = (object) $this->upload->data();
					$obj = array(
						'thumbnail_name' => (isset($image_data) && $image_data->file_name) ? $image_data->file_name : '',
						'thumbnail_type' => (isset($image_data) && $image_data->file_type) ? $image_data->file_type : '',
						'thumbnail_path' => (isset($image_data) && $image_data->full_path) ? $image_data->full_path : '',
						'name' => $req_obj->categoryName,
						'description' => $req_obj->description,
						'status' => 0,
						'created_at' => date("Y-m-d H:i:s"),
						'updated_at' => date("Y-m-d H:i:s")
					);

					$new_id = $this->category_model->new_record($obj);

						if ($new_id && $new_id > 0) {
								return _response_obj(200, NEW_RECORD_SUCCESS, null);
						} else {
							return _response_obj(500, SOMETHING_WENT_WORNG, null);
						}
				} else {
					return _response_obj(500, FILE_UPLOAD_FAIL, null);
				}							
			} else {
				return _response_obj(400, FILE_IS_REQUIRED, null);
			}
		} else {
			return _response_obj(400, REQUEST_KEYS_FAILURE, null);
		}
	}

	public function _update_category($admin_id, $product_id) {
		$put_data = file_get_contents("php://input");
		$req_obj = json_decode($put_data);
		$data = $this->category_model->check_record($admin_id, $product_id, $req_obj->productName);
		if ($data && count($data) > 0) {
			return _response_obj(400, (strtoupper($req_obj->productName) . RECORD_EXIST), null);
		} else {
			$obj = array(
				'name' => $req_obj->productName,
				'price' => $req_obj->productPrice,
				'offer' => $req_obj->productOffer,
				'pack_weight' => $req_obj->productPackweight,
				'stock_weight' => $req_obj->productPackStock,
				'status' => 0,
			);

			$result = $this->category_model->update_record($admin_id, $product_id, $obj);
			if ($result) {
				return _response_obj(200, RECORD_UPDATE_SUCCESS, null);
			} else {
				return _response_obj(500, SOMETHING_WENT_WORNG, null);
			}
		}
	}

	public function _delete_product($admin_id, $product_id) {
		$count = $this->category_model->delete_record($admin_id, $product_id);

		if ($count && count($count) > 0) {
			$obj = array('status' => 1);
			$result = $this->category_model->update_record($admin_id, $product_id, $obj);
			if ($result) {
				return _response_obj(200, DELETE_SUCCESS, null);
			} else {
				return _response_obj(500, SOMETHING_WENT_WORNG, null);
			}
		} else {
			return _response_obj(500, RECORD_NOT_FOUND, null);
		}
	}

}

/* End of file Categories.php */
/* Location: ./application/controllers/Categories.php */