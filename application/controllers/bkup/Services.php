<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Services extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('service_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function function_router($type, $val) {
		if ($type == 'countries') {
			$result = $this->service_model->get_countries();
			return _response_obj(200, "", $result);
		} else if ($type == 'states') {
			$result = $this->service_model->get_states_by_country($val);
			return _response_obj(200, "", $result);
		} else if ($type == 'cities') {
			$result = $this->service_model->get_cities_by_state($val);
			return _response_obj(200, "", $result);
		} else {
			return _response_obj(400, QUERY_PARAM_MISSING, null);
		}	
	}
}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */