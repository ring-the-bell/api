<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Stores extends CI_Controller {

	public function __construct() {		
		parent::__construct();
		$this->load->model('store_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function function_router() {
		$method_type = $_SERVER["REQUEST_METHOD"];

		if (isset($method_type)) {
			if ($method_type == "GET") {
				# code...
			} else if ($method_type == "POST") {
				if ($this->input->request_headers()) {
					if (_verify_session_token($this->input->request_headers())) {
						return _response_obj(400, SESSION_EXIST, null);
					} else {
						$obj = (object) $this->input->post();
						$this->new_store($obj);					
					}
				} else {
					return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
				}
			}
		} else {
			return _response_obj(400, REQUEST_METHOD_FAILURE, null);
		}
	}

	public function new_store($reqObj) {

		// if ((count($_FILES) > 0 )&& (count($this->input->post())) > 0) {

		if (count($this->input->post()) > 0 ) {
			if (!isset($reqObj->store_name)) {
				return _response_obj(400, REQUEST_KEYS_FAILURE, null);
			} else {
				if (_verify_record('stores', 'name', $reqObj->store_name)) {
					return _response_obj(400, (strtoupper($reqObj->store_name) . RECORD_EXIST), null);
				}
			}



			if (!isset($reqObj->store_number)) {
				return _response_obj(400, REQUEST_KEYS_FAILURE, null);
			} else {
				if (_verify_record('stores', 'contact_no', $reqObj->store_number)) {
					return _response_obj(400, ($reqObj->store_number . RECORD_EXIST), null);
				}
			}

			if (!isset($reqObj->store_location)) {
				return _response_obj(400, REQUEST_KEYS_FAILURE, null);
			}

			if (!isset($reqObj->password)) {
				return _response_obj(400, REQUEST_KEYS_FAILURE, null);
			}

			if (!isset($reqObj->email_id)) {
				$reqObj->email_id = '';
			} else {
				if (_verify_record('stores', 'email', $reqObj->email_id)) {
					return _response_obj(400, (strtoupper($reqObj->email_id) . RECORD_EXIST), null);
				}
			}

			if (isset($_FILES) && isset($_FILES['logo'])) {
				// var_dump($_FILES['logo']);
				$upload_path = './uploads/stores';
				//file upload destination
				$config['upload_path'] = $upload_path;
				//allowed file types. * means all types
				$config['allowed_types'] = '*';
				//allowed max file size. 0 means unlimited file size
				$config['max_size'] = '0';
				//max file name size
				$config['max_filename'] = '255';
				//whether file name should be encrypted or not
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config);

				if (!file_exists($upload_path)) {
					mkdir($upload_path, 0777, true);
				}
				
				if($this->upload->do_upload('logo')) {
					$logo_data = (object) $this->upload->data();
					// var_dump($logo_data);
				} else {
					return _response_obj(500, FILE_UPLOAD_FAIL, null);
				}
			} 

			$obj = array(
				'logo_name' => (isset($logo_data) && $logo_data->file_name) ? $logo_data->file_name : '',
				'logo_type' => (isset($logo_data) && $logo_data->file_type) ? $logo_data->file_type : '',
				'logo_path' => (isset($logo_data) && $logo_data->full_path) ? $logo_data->full_path : '',
				'name' => $reqObj->store_name,
				'contact_no' => $reqObj->store_number,
				'email' => $reqObj->email_id,
				'password_hash' => password_hash($reqObj->password, PASSWORD_DEFAULT),
				'address' => $reqObj->store_location,
				'status' => 0,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s")
			);

			$new_id = $this->store_model->new_record($obj);

			if ($new_id && $new_id > 0) {
				$tokenData = array();
		        $tokenData['id'] = $new_id; 
		        $tokenData['timestamp'] = now();
		        $output['token'] = AUTHORIZATION::generateToken($tokenData);
		 		$token = _session_key_enc_dec_ryption($output['token'], 'e');
		 		return _response_obj(200, SIGNUP_SUCCESS, array('access_token' =>  _session_key_enc_dec_ryption($output['token'], 'e')));
			} else {
				return _response_obj(500, SOMETHING_WENT_WORNG, null);
			}

		} else {
			return _response_obj(400, REQUEST_KEYS_FAILURE, null);
		}
	}

	public function check_session() {
		// var_dump($_SERVER["REQUEST_METHOD"]);
		// var_dump(_verify_session_token($this->input->request_headers()));
		// // var_dump(explode("Bearer ", $this->input->request_headers()['Authorization']));
		// $headers = $this->input->request_headers();
		// // var_dump(explode("Bearer ", $headers['Authorization']));
		// $encp_key = explode("Bearer ", $headers['Authorization'])[1];
		// // return;

		// if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
  //           $decodedToken = AUTHORIZATION::validateToken(_session_key_enc_dec_ryption($encp_key, 'd'));
  //           if ($decodedToken != false) {
  //               echo 'match';
  //               return;
  //           } else {
  //           	echo 'fail';
  //           }
  //       }
	}
}

/* End of file Stores.php */
/* Location: ./application/controllers/Stores.php */