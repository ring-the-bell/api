<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function function_router($type) {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$store_id = _decrypt_session_token($this->input->request_headers());
				$role_id = _get_role_id('stores', $store_id);
				if ($role_id && $role_id > 0) {
					if (_verify_role('retailer', $role_id)) {
						if (isset($store_id) && $store_id > 0) {
							$method_type = $_SERVER["REQUEST_METHOD"];
							if (isset($method_type)) {
								if ($method_type == "GET") {
									if (isset($type)) {
										if ($type == 'products') {
											$this->_get_products_count($store_id);
										} else if ($type == 'orders') {
											$this->_get_orders_count($store_id);
										} else {
											return _response_obj(400, INVALID_PARAM, null);
										}
									} else {
										return _response_obj(400, QUERY_PARAM_MISSING, null);
									}
								} else {
									return _response_obj(400, INCORECT_REQUEST_METHOD, null);
								}
							} else {
								return _response_obj(400, REQUEST_METHOD_FAILURE, null);
							}
						} else {
							return _response_obj(401, SESSION_FAILURE, null);
						}
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function _get_products_count($store_id) {
		$result = $this->dashboard_model->get_products_count($store_id);
		return _response_obj(200, null, count($result));
	}

	public function _get_orders_count($store_id) {
		$result = $this->dashboard_model->get_orders_count($store_id);
		return _response_obj(200, null, count($result));
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */