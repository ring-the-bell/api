<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('store_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function function_router($app_type) {
		if ($app_type) {
			if ($app_type == 'mobile') {
				$post_data = file_get_contents("php://input");
				$req_obj = json_decode($post_data);
			} else if ($app_type == 'web') {
				$req_obj = (object) $_POST;
			}

			if ($req_obj) {
				if (!isset($req_obj->userType)) {
					return _response_obj(400, ('userType' . KEY_FAILURE), null);
				} else {
					if (($req_obj->userType == 'user') || ($req_obj->userType == 'shopper')) {

						if (!isset($req_obj->fullName)) {
							return _response_obj(400, ('fullName' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->mobileNumber)) {
							return _response_obj(400, ('mobileNumber' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->address)) {
							return _response_obj(400, ('address' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->password)) {
							return _response_obj(400, ('password' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->country)) {
							return _response_obj(400, ('country' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->state)) {
							return _response_obj(400, ('state' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->city)) {
							return _response_obj(400, ('city' . KEY_FAILURE), null);
						}

						if (!isset($req_obj->email)) {
							$req_obj->email = '';
						}

						$new_obj = array(
							"name" => $req_obj->fullName,
							"contact_no" => $req_obj->mobileNumber,
							"address" => $req_obj->address,
							"email" => $req_obj->email,
							"password_hash" => password_hash($req_obj->password, PASSWORD_DEFAULT),
							"country" => $req_obj->country,
							"state" => $req_obj->state,
							"city" => $req_obj->city
						);

						if ($req_obj->userType == 'shopper') {
							$new_obj['role_id'] = 2;			
						} else {
							$new_obj['role_id'] = 3;
						}

						$new_obj['status'] = 0;
						$new_obj['created_at'] = date("Y-m-d H:i:s");
						$new_obj['updated_at'] = date("Y-m-d H:i:s");
						
						$user_id = $this->user_model->new_user($new_obj);

						if ($user_id && $user_id > 0) {
							if ($req_obj->userType == 'shopper') {
								if (isset($_FILES) && isset($_FILES['logo'])) {
									$upload_path = './uploads/stores';
									//file upload destination
									$config['upload_path'] = $upload_path;
									//allowed file types. * means all types
									$config['allowed_types'] = '*';
									//allowed max file size. 0 means unlimited file size
									$config['max_size'] = '0';
									//max file name size
									$config['max_filename'] = '255';
									//whether file name should be encrypted or not
									$config['encrypt_name'] = TRUE;
									 
									$this->load->library('upload', $config);

									if (!file_exists($upload_path)) {
										mkdir($upload_path, 0777, true);
									}

									if($this->upload->do_upload('logo')) {
										$logo_data = (object) $this->upload->data();
										$obj = array(
											'user_id' => $user_id,
											'name' => $req_obj->storeName,
											'logo_name' => (isset($logo_data) && $logo_data->file_name) ? $logo_data->file_name : '',
											'logo_type' => (isset($logo_data) && $logo_data->file_type) ? $logo_data->file_type : '',
											'logo_path' => (isset($logo_data) && $logo_data->full_path) ? $logo_data->full_path : '',
											'status' => 0,
											'created_at' => date("Y-m-d H:i:s"),
											'updated_at' => date("Y-m-d H:i:s")
										);

										$store_id = $this->store_model->new_store($obj);

										if ($store_id && $store_id > 0) {
											return _response_obj(200, SIGNUP_SUCCESS, null);
										} else {
											return _response_obj(500, SOMETHING_WENT_WORNG, null);
										}
									} else {
										return _response_obj(500, FILE_UPLOAD_FAIL, null);
									}						
								} else {
									return _response_obj(400, FILE_IS_REQUIRED, null);
								}
							} else {
								return _response_obj(200, SIGNUP_SUCCESS, null);					
							}			
						} else {
							return _response_obj(500, SOMETHING_WENT_WORNG, null);
						}
					} else {
						return _response_obj(400, ('userType' . KEY_FAILURE), null);
					}
				}
			} else {
				return _response_obj(400, file_get_contents("php://input"), $_FILES);
			}
		}
	}
}

/* End of file Users.php */
/* Location: ./application/models/Users.php */