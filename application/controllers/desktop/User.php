<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('store_model');
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function new_customer() {
		$post_data = file_get_contents("php://input");
		$req_obj = json_decode($post_data);

		if (isset($req_obj)) {
			if (!isset($req_obj->userType)) {
				return _response_obj(400, ('userType' . KEY_FAILURE), null);
			} else if ($req_obj->userType != 'shopper') {
				return _response_obj(400, INVALID_PARAM, null);
			} else {
				if (!isset($req_obj->fullName)) {
					return _response_obj(400, ('fullName' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->fullName)) {
						return _response_obj(400, ('fullName' . EMPTY_VALUE), null);
					} else {
						$name_exist = _verify_record('users', 'name', $req_obj->fullName);

						if ($name_exist) {
							return _response_obj(400, ('name' . RECORD_EXIST), null);
						}
					}
				}

				if (!isset($req_obj->mobileNumber)) {
					return _response_obj(400, ('mobileNumber' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->mobileNumber)) {
						return _response_obj(400, ('mobileNumber' . EMPTY_VALUE), null);
					} else {
						$number_exist = _verify_record('users', 'contact_no', $req_obj->mobileNumber);

						if ($number_exist) {
							return _response_obj(400, ('mobile Number' . RECORD_EXIST), null);
						}
					}
				} 

				if (!isset($req_obj->address)) {
					return _response_obj(400, ('address' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->address)) {
						return _response_obj(400, ('address' . EMPTY_VALUE), null);
					}
				}

				if (!isset($req_obj->password)) {
					return _response_obj(400, ('password' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->password)) {
						return _response_obj(400, ('password' . EMPTY_VALUE), null);
					}
				}

				// if (!isset($req_obj->country)) {
				// 	return _response_obj(400, ('country' . KEY_FAILURE), null);
				// } else {
				// 	if (empty($req_obj->country)) {
				// 		return _response_obj(400, ('country' . EMPTY_VALUE), null);
				// 	}
				// }

				if (!isset($req_obj->state)) {
					return _response_obj(400, ('state' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->state)) {
						return _response_obj(400, ('state' . EMPTY_VALUE), null);
					}
				}

				if (!isset($req_obj->city)) {
					return _response_obj(400, ('city' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->city)) {
						return _response_obj(400, ('city' . EMPTY_VALUE), null);
					}
				}

				if (!isset($req_obj->email)) {
					$req_obj->email = '';
				} else {
					if (empty($req_obj->email)) {
						return _response_obj(400, ('email' . EMPTY_VALUE), null);
					} else {
						$email_exist = _verify_record('users', 'email', $req_obj->email);

						if ($email_exist) {
							return _response_obj(400, ('email' . RECORD_EXIST), null);
						}						
					}
				}

				// if (!isset($req_obj->latitude)) {
				// 	return _response_obj(400, ('latitude' . KEY_FAILURE), null);
				// } else {
				// 	if (empty($req_obj->latitude)) {
				// 		return _response_obj(400, ('latitude' . EMPTY_VALUE), null);
				// 	}
				// }

				// if (!isset($req_obj->langitude)) {
				// 	return _response_obj(400, ('langitude' . KEY_FAILURE), null);
				// } else {
				// 	if (empty($req_obj->langitude)) {
				// 		return _response_obj(400, ('langitude' . EMPTY_VALUE), null);
				// 	}
				// }

				$new_obj = array(
					"name" => $req_obj->fullName,
					"contact_no" => $req_obj->mobileNumber,
					"address" => $req_obj->address,
					"email" => $req_obj->email,
					"password_hash" => password_hash($req_obj->password, PASSWORD_DEFAULT),
					// "country" => 101,
					"state" => $req_obj->state,
					"city" => $req_obj->city,
					"role_id" => 3,
					"status" => 0,
					"created_at" => date("Y-m-d H:i:s"),
					"updated_at" => date("Y-m-d H:i:s")
				);
				
				$user_id = $this->user_model->new_user($new_obj);

				if ($user_id && $user_id > 0) {
					$data = array(
						"address"=> $req_obj->address,
						"cityId"=> $req_obj->city,
						"email"=> $req_obj->email,
						"fullName"=> $req_obj->fullName,
						"mobileNumber"=> $req_obj->mobileNumber,
						"stateId"=> $req_obj->state,
						"userId"=> $user_id,
						"userType"=> $req_obj->userType,
						// "latitude" => $req_obj->latitude,
						// "langitude" => $req_obj->langitude,
					);
					$tokenData = array();
			        $tokenData['id'] = $user_id; 
			        $tokenData['timestamp'] = now();
			        $output['token'] = AUTHORIZATION::generateToken($tokenData);
			 		$token = _session_key_enc_dec_ryption($output['token'], 'e');
			 		return _response_obj(200, SIGNUP_SUCCESS, array('access_token' =>  _session_key_enc_dec_ryption($output['token'], 'e'), 'data' => $data));						
				} else {
					return _response_obj(500, SOMETHING_WENT_WORNG, null);
				}					
			}
		} else {
			return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		}
	}

	public function new_store() {
		$req_obj = (object) $_POST;
		// var_dump($req_obj, $_FILES['storeLogo']);
		// return;

		if (isset($req_obj)) {
			if (!isset($req_obj->userType)) {
				return _response_obj(400, ('userType' . KEY_FAILURE), null);
			} else if ($req_obj->userType != 'retailer') {
				return _response_obj(400, INVALID_PARAM, null);
			} else {
				if (!isset($req_obj->storeName)) {
					return _response_obj(400, ('storeName' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->storeName)) {
						return _response_obj(400, ('storeName' . EMPTY_VALUE), null);
					} else {
						$store_exist = _verify_record('stores', 'name', $req_obj->storeName);

						if ($store_exist) {
							return _response_obj(400, ('store name' . RECORD_EXIST), null);
						}
					} 
				} 

				if (!isset($req_obj->fullName)) {
					return _response_obj(400, ('fullName' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->fullName)) {
						return _response_obj(400, ('fullName' . EMPTY_VALUE), null);
					} else {
						$name_exist = _verify_record('users', 'name', $req_obj->fullName);

						if ($name_exist) {
							return _response_obj(400, ('name' . RECORD_EXIST), null);
						}
					}
				} 

				if (!isset($req_obj->mobileNumber)) {
					return _response_obj(400, ('mobileNumber' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->mobileNumber)) {
						return _response_obj(400, ('mobileNumber' . EMPTY_VALUE), null);
					} else {
						$number_exist = _verify_record('users', 'contact_no', $req_obj->mobileNumber);

						if ($number_exist) {
							return _response_obj(400, ('mobile Number' . RECORD_EXIST), null);
						}
					}
				} 

				if (!isset($req_obj->address)) {
					return _response_obj(400, ('address' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->address)) {
						return _response_obj(400, ('address' . EMPTY_VALUE), null);
					}
				}

				if (!isset($req_obj->password)) {
					return _response_obj(400, ('password' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->password)) {
						return _response_obj(400, ('password' . EMPTY_VALUE), null);
					}
				}

				// if (!isset($req_obj->country)) {
				// 	return _response_obj(400, ('country' . KEY_FAILURE), null);
				// } else {
				// 	if (empty($req_obj->country)) {
				// 		return _response_obj(400, ('country' . EMPTY_VALUE), null);
				// 	}
				// }

				if (!isset($req_obj->state)) {
					return _response_obj(400, ('state' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->state)) {
						return _response_obj(400, ('state' . EMPTY_VALUE), null);
					}
				}

				if (!isset($req_obj->city)) {
					return _response_obj(400, ('city' . KEY_FAILURE), null);
				} else {
					if (empty($req_obj->city)) {
						return _response_obj(400, ('city' . EMPTY_VALUE), null);
					}
				}

				if (isset($req_obj->delivery)) {
					if (!isset($req_obj->amount)) {
						return _response_obj(400, ('amount' . KEY_FAILURE), null);
					}
					if (!isset($req_obj->charges)) {
						return _response_obj(400, ('charges' . KEY_FAILURE), null);
					}
				}

				if (!isset($req_obj->email)) {
					$req_obj->email = '';
				} else {
					if (empty($req_obj->email)) {
						return _response_obj(400, ('email' . EMPTY_VALUE), null);
					} else {
						$email_exist = _verify_record('users', 'email', $req_obj->email);

						if ($email_exist) {
							return _response_obj(400, ('email' . RECORD_EXIST), null);
						}
					}
				}

				// if (!isset($req_obj->latitude)) {
				// 	return _response_obj(400, ('latitude' . KEY_FAILURE), null);
				// } else {
				// 	if (empty($req_obj->latitude)) {
				// 		return _response_obj(400, ('latitude' . EMPTY_VALUE), null);
				// 	}
				// }

				// if (!isset($req_obj->langitude)) {
				// 	return _response_obj(400, ('langitude' . KEY_FAILURE), null);
				// } else {
				// 	if (empty($req_obj->langitude)) {
				// 		return _response_obj(400, ('langitude' . EMPTY_VALUE), null);
				// 	}
				// } 

				$new_obj = array(
					"name" => $req_obj->fullName,
					"contact_no" => $req_obj->mobileNumber,
					"address" => $req_obj->address,
					"email" => $req_obj->email,
					"password_hash" => password_hash($req_obj->password, PASSWORD_DEFAULT),
					// "country" => $req_obj->country,
					"state" => $req_obj->state,
					"city" => $req_obj->city,
					"role_id" => 2,
					"status" => 0,
					"created_at" => date("Y-m-d H:i:s"),
					"updated_at" => date("Y-m-d H:i:s")
				);
				
				$user_id = $this->user_model->new_user($new_obj);

				if ($user_id && $user_id > 0) {
					if (isset($_FILES) && isset($_FILES['storeLogo'])) {
						// var_dump($_FILES['logo']);
						$upload_path = './uploads/stores';
						//file upload destination
						$config['upload_path'] = $upload_path;
						//allowed file types. * means all types
						$config['allowed_types'] = 'jpeg|jpg|png';
						//allowed max file size. 0 means unlimited file size
						$config['max_size'] = '0';
						//max file name size
						$config['max_filename'] = '255';
						//whether file name should be encrypted or not
						$config['encrypt_name'] = TRUE;

						$file_types = array("image/jpeg", "image/png", "image/jpg");

						if (!in_array($_FILES['storeLogo']['type'], $file_types)) {
							return _response_obj(400, INCORRECT_FILE_FORMAT, null);
						} else {
							$this->load->library('upload', $config);

							if (!file_exists($upload_path)) {
								mkdir($upload_path, 0777, true);
							}
							
							if($this->upload->do_upload('storeLogo')) {
								$logo_data = (object) $this->upload->data();
								// var_dump($logo_data);
								$img_path = explode("/home/c8ugqa8m8lxk/public_html/api/", $logo_data->full_path);
								
								$obj = array(
									'user_id' => $user_id,
									'name' => $req_obj->storeName,
									'logo' => (isset($img_path[1]) && $logo_data->full_path) ? ("http://ringthebell.in/api/" . $img_path[1]) : $logo_data->full_path,
									'delivery' => $req_obj->delivery == 'true' ? 0 : 1,
									'min_amount' => $req_obj->amount ? $req_obj->amount : '',
									'charges' => $req_obj->charges ? $req_obj->charges : '',
									'status' => 0,
									'plan_type' => 0,
									'created_at' => date("Y-m-d H:i:s"),
									'updated_at' => date("Y-m-d H:i:s"),
									'end_date' => date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' + 30 days'))
								);

								$store_id = $this->store_model->new_store($obj);

								if ($store_id && $store_id > 0) {
									$data = array(
										"address" => $req_obj->address,
										"cityId" => $req_obj->city,
										"email" => $req_obj->email,
										"fullName" => $req_obj->fullName,
										"mobileNumber" => $req_obj->mobileNumber,
										"stateId" => $req_obj->state,
										"userId" => $user_id,
										"userType" => $req_obj->userType,
										"storeName" => $req_obj->storeName,
										// "latitude" => $req_obj->latitude,
										// "langitude" => $req_obj->langitude,
									);
									$tokenData = array();
							        $tokenData['id'] = $user_id; 
							        $tokenData['timestamp'] = now();
							        $output['token'] = AUTHORIZATION::generateToken($tokenData);
							 		$token = _session_key_enc_dec_ryption($output['token'], 'e');
							 		return _response_obj(200, SIGNUP_SUCCESS, array('access_token' =>  _session_key_enc_dec_ryption($output['token'], 'e'), 'data' => $data));
								} else {
									return _response_obj(500, SOMETHING_WENT_WORNG, null);
								}		
							} else {
								return _response_obj(500, FILE_UPLOAD_FAIL, null);
							}							
						}						
					} else {
						return _response_obj(400, FILE_IS_REQUIRED, null);
					} 
				} else {
					return _response_obj(500, SOMETHING_WENT_WORNG, null);
				}					
			}
		} else {
			return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/desktop/User.php */