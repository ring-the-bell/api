<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/third_party/REST_Controller.php';

class Product extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		return _response_obj(403, UN_AUTHORIZED, null);
	}

	public function new_product() {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$store_id = _decrypt_session_token($this->input->request_headers());
				$role_id = _get_role_id('stores', $store_id);
				if ($role_id && $role_id > 0) {
					if (_verify_role('retailer', $role_id)) {
						if (isset($store_id) && $store_id > 0) {
							if (isset($_POST)) {
								if (isset($_POST['obj'])) {
									$post_data = $_POST['obj'];
									$req_obj = json_decode($post_data);

									if (_verify_record('stores', 'id', $store_id)) {
										if (!isset($req_obj->productCategoryId)) {
											return _response_obj(400, ('productCategoryId' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productCategoryId)) {
												return _response_obj(400, ('productCategoryId' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productName)) {
											return _response_obj(400, ('productName' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productName)) {
												return _response_obj(400, ('productName' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productPrice)) {
											return _response_obj(400, ('productPrice' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPrice)) {
												return _response_obj(400, ('productPrice' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productOffer)) {
											return _response_obj(400, ('productOffer' . KEY_FAILURE), null);
										}

										if (!isset($req_obj->productPackWeight)) {
											return _response_obj(400, ('productPackWeight' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPackWeight)) {
												return _response_obj(400, ('productPackWeight' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productPackType)) {
											return _response_obj(400, ('productPackType' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPackType)) {
												return _response_obj(400, ('productPackType' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productPackStock)) {
											return _response_obj(400, ('productPackStock' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPackStock)) {
												return _response_obj(400, ('productPackStock' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productStatus)) {
											return _response_obj(400, ('productStatus' . KEY_FAILURE), null);
										}

										if (isset($req_obj->productOffer) && $req_obj->productOffer) {
											$req_obj->offerPrice = $req_obj->productPrice - (($req_obj->productOffer/100 ) * $req_obj->productPrice);
										} else {
											$req_obj->offerPrice = $req_obj->productPrice;
										}


										$result = $this->product_model->check_record_on_add($store_id, $req_obj->productPackWeight, $req_obj->productName);

										if ($result && count($result) > 0) {
											return _response_obj(409, COMBO_ISSUE, null);
										} else {
											if (isset($_FILES) && isset($_FILES['image'])) {
												// var_dump($_FILES['logo']);
												$upload_path = './uploads/products';
												//file upload destination
												$config['upload_path'] = $upload_path;
												//allowed file types. * means all types
												$config['allowed_types'] = '*';
												//allowed max file size. 0 means unlimited file size
												$config['max_size'] = '0';
												//max file name size
												$config['max_filename'] = '255';
												//whether file name should be encrypted or not
												$config['encrypt_name'] = TRUE;
												
												$this->load->library('upload', $config);

												if (!file_exists($upload_path)) {
													mkdir($upload_path, 0777, true);
												}

												if($this->upload->do_upload('image')) {
													$image_data = (object) $this->upload->data();
													$img_path = explode("/home/c8ugqa8m8lxk/public_html/api/", $image_data->full_path);
													$obj = array(
														'thumbnail_name' => (isset($image_data) && $image_data->file_name) ? $image_data->file_name : '',
														'thumbnail_type' => (isset($image_data) && $image_data->file_type) ? $image_data->file_type : '',
														'thumbnail_path' => (isset($img_path[1]) && $image_data->full_path) ? ("http://ringthebell.in/api/" . $img_path[1]) : $image_data->full_path,
														'store_id' => $store_id,
														'category_id' => $req_obj->productCategoryId,
														'name' => $req_obj->productName,
														'price' => $req_obj->productPrice,
														'offer' => $req_obj->productOffer,
														'offer_price' => $req_obj->offerPrice,
														'type' => $req_obj->productPackType,
														'pack_weight' => $req_obj->productPackWeight,
														'stock_weight' => $req_obj->productPackStock,
														'status' => $req_obj->productStatus,
													);

													$new_id = $this->product_model->new_record($obj);

														if ($new_id && $new_id > 0) {
																return _response_obj(200, NEW_RECORD_SUCCESS, null);
														} else {
															return _response_obj(500, SOMETHING_WENT_WORNG, null);
														}
												} else {
													return _response_obj(500, FILE_UPLOAD_FAIL, null);
												}
											}
										}
									} else {
										return _response_obj(401, SESSION_FAILURE, null);
									}				
								} else {
									return _response_obj(400, ('obj ' . KEY_FAILURE), null);
								}
							} else {
								return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
							}
						} else {
							return _response_obj(401, SESSION_FAILURE, null);
						}						
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function update_product($product_id) {
		if ($this->input->request_headers() && isset($this->input->request_headers()['Authorization'])) {
			if (_verify_session_token($this->input->request_headers())) {
				$store_id = _decrypt_session_token($this->input->request_headers());
				$role_id = _get_role_id('stores', $store_id);
				if ($role_id && $role_id > 0) {
					if (_verify_role('retailer', $role_id)) {
						if (isset($store_id) && $store_id > 0) {
							if (isset($_POST)) {
								if (isset($_POST['obj'])) {
									$post_data = $_POST['obj'];
									$req_obj = json_decode($post_data);

									$data = $this->product_model->check_record_on_update($store_id, $product_id, $req_obj->productName, $req_obj->productPackWeight);
									if ($data && count($data) > 0) {
										return _response_obj(409, COMBO_ISSUE, null);
									} else {
										if (!isset($req_obj->productCategoryId)) {
											return _response_obj(400, ('productCategoryId' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productCategoryId)) {
												return _response_obj(400, ('productCategoryId' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productName)) {
											return _response_obj(400, ('productName' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productName)) {
												return _response_obj(400, ('productName' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productPrice)) {
											return _response_obj(400, ('productPrice' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPrice)) {
												return _response_obj(400, ('productPrice' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productOffer)) {
											return _response_obj(400, ('productOffer' . KEY_FAILURE), null);
										}

										if (!isset($req_obj->productPackWeight)) {
											return _response_obj(400, ('productPackWeight' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPackWeight)) {
												return _response_obj(400, ('productPackWeight' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productPackType)) {
											return _response_obj(400, ('productPackType' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPackType)) {
												return _response_obj(400, ('productPackType' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productPackStock)) {
											return _response_obj(400, ('productPackStock' . KEY_FAILURE), null);
										} else {
											if (empty($req_obj->productPackStock)) {
												return _response_obj(400, ('productPackStock' . EMPTY_VALUE), null);
											}
										}

										if (!isset($req_obj->productStatus)) {
											return _response_obj(400, ('productStatus' . KEY_FAILURE), null);
										}

										if (isset($req_obj->productOffer) && $req_obj->productOffer) {
											$req_obj->offerPrice = $req_obj->productPrice - (($req_obj->productOffer/100 ) * $req_obj->productPrice);
										} else {
											$req_obj->offerPrice = $req_obj->productPrice;
										}

										if (isset($_FILES) && isset($_FILES['image'])) {
											// var_dump($_FILES['logo']);
											$upload_path = './uploads/products';
											//file upload destination
											$config['upload_path'] = $upload_path;
											//allowed file types. * means all types
											$config['allowed_types'] = '*';
											//allowed max file size. 0 means unlimited file size
											$config['max_size'] = '0';
											//max file name size
											$config['max_filename'] = '255';
											//whether file name should be encrypted or not
											$config['encrypt_name'] = TRUE;
											
											$this->load->library('upload', $config);

											if (!file_exists($upload_path)) {
												mkdir($upload_path, 0777, true);
											}

											if($this->upload->do_upload('image')) {
												$image_data = (object) $this->upload->data();
												$img_path = explode("/home/c8ugqa8m8lxk/public_html/api/", $image_data->full_path);
												$obj = array(
													'thumbnail_name' => (isset($image_data) && $image_data->file_name) ? $image_data->file_name : '',
													'thumbnail_type' => (isset($image_data) && $image_data->file_type) ? $image_data->file_type : '',
													'thumbnail_path' => (isset($img_path[1]) && $image_data->full_path) ? ("http://ringthebell.in/api/" . $img_path[1]) : $image_data->full_path,
													'store_id' => $store_id,
													'category_id' => $req_obj->productCategoryId,
													'name' => $req_obj->productName,
													'price' => $req_obj->productPrice,
													'offer' => $req_obj->productOffer,
													'offer_price' => $req_obj->offerPrice,
													'type' => $req_obj->productPackType,
													'pack_weight' => $req_obj->productPackWeight,
													'stock_weight' => $req_obj->productPackStock,
													'status' => $req_obj->productStatus,
												);
												$this->_update_product_by_type($acc_details->store_id, $product_id, $obj);
											} else {
												return _response_obj(500, FILE_UPLOAD_FAIL, null);
											}
										} else {
											$obj = array(
													'store_id' => $store_id,
													'category_id' => $req_obj->productCategoryId,
													'name' => $req_obj->productName,
													'price' => $req_obj->productPrice,
													'offer' => $req_obj->productOffer,
													'offer_price' => $req_obj->offerPrice,
													'type' => $req_obj->productPackType,
													'pack_weight' => $req_obj->productPackWeight,
													'stock_weight' => $req_obj->productPackStock,
													'status' => $req_obj->productStatus,
												);
											$this->_update_product_by_type($store_id, $product_id, $obj);
										}		
									}				
								} else {
									return _response_obj(400, ('obj ' . KEY_FAILURE), null);
								}
							} else {
								return _response_obj(400, REQUEST_OBJECT_FAILURE, null);
							}
						} else {
							return _response_obj(401, SESSION_FAILURE, null);
						}						
					} else {
						return _response_obj(403, UN_AUTHORIZED, null);
					}
				} else {
					return _response_obj(403, UN_AUTHORIZED, null);
				}
			} else {
				return _response_obj(401, SESSION_FAILURE, null);					
			}
		} else {
			return _response_obj(400, REQUEST_HEADERS_FAILURE, null);
		}
	}

	public function _update_product_by_type($store_id, $product_id, $obj) {
		$result = $this->product_model->update_record($store_id, $product_id, $obj);
		if ($result) {
			return _response_obj(200, RECORD_UPDATE_SUCCESS, null);
		} else {
			return _response_obj(500, SOMETHING_WENT_WORNG, null);
		}
	}
}

/* End of file Product.php */
/* Location: ./application/controllers/android/Product.php */