<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function verify_subscriptions() {
		// /usr/local/bin/php -q /home/c8ugqa8m8lxk/public_html/api/application/controllers/welcome/verify_subscriptions
		$present_date = date("Y-m-d H:i:s");
		$rcrds = $this->db->select('id as store_id, end_date as subscriptionDate')
				->where('status', 0)
				->get('stores')
				->result();
		if (isset($rcrds) && $rcrds && count($rcrds) > 0) {
			foreach ($rcrds as $key => $value) {
				if ($value->subscriptionDate < $present_date) {
					$this->db->update('stores', array('status' => 1), array('id' => $value->store_id));
				}
			}
		}
	}
}
