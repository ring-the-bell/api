<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';

/* ------------------- MOBILE -------------------- */
// signup/register
$route['mobile/user/customer'] = 'android/user/new_customer';
$route['mobile/user/store'] = 'android/user/new_store';

/* ------------------- WEB ----------------------- */
// signup/register
$route['web/user/store'] = 'desktop/user/new_store';
$route['web/user/customer'] = 'desktop/user/new_customer';

// products
$route['web/product/new'] = 'desktop/product/new_product';
$route['web/product/update/(:any)'] = 'desktop/product/update_product/$1';

/*--------------- ALL DEVICES --------------*/
// deployment
$route['web/site/updates'] = 'shared/utils/get_deployment_detaills';

// subscription
$route['web/site/subscription/plans'] = 'shared/utils/get_subscription_details';

// store expr
$route['web/store/validity'] = 'shared/utils/check_dates';

// contact us
$route['web/user/suggestions/'] = 'shared/utils/new_suggestion';

// auth
$route['mobile/auth'] = 'shared/auth/user_sign_in';
$route['web/auth'] = 'shared/auth/user_sign_in';

// categories
$route['mobile/categories'] = 'shared/utils/get_categories';
$route['web/categories'] = 'shared/utils/get_categories';

// states
$route['mobile/states'] = 'shared/locations/get_states_by_country';
$route['web/states'] = 'shared/locations/get_states_by_country';

// cities
$route['mobile/cities/(:any)'] = 'shared/locations/get_cities_by_state/$1';
$route['web/cities/(:any)'] = 'shared/locations/get_cities_by_state/$1';

// store by location
$route['mobile/stores/location'] = 'shared/utils/get_all_stores_by_location';
$route['web/stores/location'] = 'shared/utils/get_all_stores_by_location';

// stores
$route['mobile/stores'] = 'shared/utils/get_all_stores';
$route['web/stores'] = 'shared/utils/get_all_stores';

// store details
$route['web/store/details/(:any)'] = 'shared/utils/get_store_details_by_id/$1';
$route['mobile/store/info'] = 'shared/utils/get_store_details';
$route['web/store/info'] = 'shared/utils/get_store_details';
$route['web/store/shipping/(:any)'] = 'shared/utils/get_store_shipping_details/$1';


// statistics
$route['web/statistics/(:any)'] = 'shared/utils/statistics/$1';

// products
// user
$route['mobile/store/products/(:any)'] = 'shared/utils/get_all_products/$1';
$route['web/store/products/(:any)'] = 'shared/utils/get_all_products/$1';
$route['web/store/products/choice/(:any)/(:any)'] = 'shared/utils/get_all_products_by_name/$1/$2';
// store
$route['web/store/products'] = 'shared/utils/get_all_products_store';
$route['web/store/product/verify/(:any)'] = 'shared/utils/check_product_availability/$1';
$route['web/store/product/search/(:any)'] = 'shared/utils/search_product/$1';


// records verification
$route['mobile/verify/storename/(:any)'] = 'shared/utils/store_name_exist/$1';
$route['web/verify/storename/(:any)'] = 'shared/utils/store_name_exist/$1';

$route['mobile/verify/username/(:any)'] = 'shared/utils/user_name_exist/$1';
$route['web/verify/username/(:any)'] = 'shared/utils/user_name_exist/$1';

$route['mobile/verify/number/(:any)'] = 'shared/utils/contact_number_exist/$1';
$route['web/verify/number/(:any)'] = 'shared/utils/contact_number_exist/$1';

$route['mobile/verify/email/(:any)'] = 'shared/utils/email_exist/$1';
$route['web/verify/email/(:any)'] = 'shared/utils/email_exist/$1';


// orders
$route['web/cart/remove/(:any)'] = 'shared/orders/remove_all/$1';
$route['web/cart/items'] = 'shared/orders/user_cart_items';

$route['web/orders/new'] = 'shared/orders/order_confirm';

$route['web/user/invoice'] = 'shared/orders/get_user_invoice';
$route['web/user/invoice/order/(:any)'] = 'shared/orders/get_invoice_order_details/$1';

$route['web/store/invoice'] = 'shared/orders/get_store_invoice';
$route['web/store/invoice/order/(:any)'] = 'shared/orders/get_store_invoice_order_details/$1';
$route['web/store/invoice/status'] = 'shared/orders/update_invoice';

$route['web/user/profile'] = 'shared/profile/user_info';

$route['web/store/profile'] = 'shared/profile/get_store_info';
$route['web/store/details'] = 'shared/profile/get_store_details';

$route['web/store/update'] = 'shared/profile/update_store';
$route['web/password/update'] = 'shared/profile/update_password';





// $route['services/(:any)'] = 'services/function_router/$1/$2';
// $route['services/(:any)/(:any)'] = 'services/function_router/$1/$2';
// $route['auth'] = 'auth/signin';
// $route['auth/new'] = 'auth/check';
// // $route['users'] = 'users/function_router';
// $route['stores'] = 'stores/function_router';
// $route['categories'] = 'categories/function_router/$1';
// $route['categories/(:any)'] = 'categories/function_router/$1';
// $route['products'] = 'products/function_router/$1';
// $route['products/(:any)'] = 'products/function_router/$1';
// $route['stats/(:any)'] = 'dashboard/function_router/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
