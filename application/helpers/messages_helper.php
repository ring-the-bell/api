<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Response Messages
|--------------------------------------------------------------------------
|
*/

defined('LOGIN_SUCCESS') OR define('LOGIN_SUCCESS', 'Logged Successfully.');
defined('LOGIN_FAILURE') OR define('LOGIN_FAILURE', 'Invalid Credentials.');
defined('LOGOUT_SUCCESS') OR define('LOGOUT_SUCCESS', 'Logout successfully.');
defined('SIGNUP_SUCCESS') OR define('SIGNUP_SUCCESS', 'Registered successfully.');
defined('NEW_RECORD_SUCCESS') OR define('NEW_RECORD_SUCCESS', 'Added Successfully.');
defined('RECORD_UPDATE_SUCCESS') OR define('RECORD_UPDATE_SUCCESS', 'Updated Successfully.');
defined('DELETE_SUCCESS') OR define('DELETE_SUCCESS', 'Deleted Successfully.');
defined('INCORRECT_PASSWORD') OR define('INCORRECT_PASSWORD', 'Please enter correct Password.');

defined('REQUEST_OBJECT_FAILURE') OR define('REQUEST_OBJECT_FAILURE', 'Request object is missing.');
defined('REQUEST_KEYS_FAILURE') OR define('REQUEST_KEYS_FAILURE', 'Request object keys are missing.');
defined('KEY_FAILURE') OR define('KEY_FAILURE', ' key is missing.');
defined('EMPTY_VALUE') OR define('EMPTY_VALUE', ' value is empty.');
defined('RECORD_NOT_FOUND') OR define('RECORD_NOT_FOUND', 'Record is not available');
defined('ACCOUNT_BLOCKED') OR define('ACCOUNT_BLOCKED', 'Sorry your account has been locked out, please use <b>Forgot Your Password</b>.<br>');
defined('SOMETHING_WENT_WORNG') OR define('SOMETHING_WENT_WORNG', 'Some thing went wrong. Please try again.');
defined('RECORD_EXIST') OR define('RECORD_EXIST', ' is already registered.');
defined('PASSWORDS_DIFFERENT_FAILURE') OR define('PASSWORDS_DIFFERENT_FAILURE', 'Old password and New password must be different.');
defined('OLD_PASSWORD_FAILURE') OR define('OLD_PASSWORD_FAILURE', 'Old password not matched.');
defined('REQUEST_HEADERS_FAILURE') OR define('REQUEST_HEADERS_FAILURE', 'Request headers are missing.');
defined('SESSION_NOT_EXIST') OR define('SESSION_NOT_EXIST', 'Invalid session.');
defined('SESSION_FAILURE') OR define('SESSION_FAILURE', 'Session expired.');
defined('FILE_UPLOAD_FAIL') OR define('FILE_UPLOAD_FAIL', 'Upload file failed. please try again.');
defined('FILE_IS_REQUIRED') OR define('FILE_IS_REQUIRED', 'Missing file. File is required');
defined('INCORRECT_FILE_FORMAT') OR define('INCORRECT_FILE_FORMAT', 'Only jpeg/jpg, and png files are allowed.');
defined('UN_AUTHORIZED') OR define('UN_AUTHORIZED', 'Access Denied for this request.');
defined('REQUEST_METHOD_FAILURE') OR define('REQUEST_METHOD_FAILURE', 'Missing REQUEST method.');
defined('SESSION_EXIST') OR define('SESSION_EXIST', 'You already registered with this details');
defined('INCORECT_REQUEST_METHOD') OR define('INCORECT_REQUEST_METHOD', 'Incorrect request method');
defined('QUERY_PARAM_MISSING') OR define('QUERY_PARAM_MISSING', 'Request params are missing');
defined('INVALID_PARAM') OR define('INVALID_PARAM', 'Request param value is invalid');
defined('COMBO_ISSUE') OR define('COMBO_ISSUE', 'This combination is already available');
defined('ADDED_IN_USER_CART') OR define('ADDED_IN_USER_CART', "Sorry, Product is added in user's cart");
defined('PRODUCT_REMOVED') OR define('PRODUCT_REMOVED', "Product is not available. Please refresh the page.");

defined('ADD_CART_SUCCESS') OR define('ADD_CART_SUCCESS', 'Added to cart.');
defined('CART_UPDATE_SUCCESS') OR define('CART_UPDATE_SUCCESS', 'Cart is updated.');
defined('CART_UPDATE_FAIL') OR define('CART_UPDATE_FAIL', 'Failed. try agian.');
defined('CART_REMOVED') OR define('CART_REMOVED', 'Cart items removed');
defined('EMPTY_CART') OR define('EMPTY_CART', "Can't place order with empty cart");
defined('STORE_NOT_FOUND') OR define('STORE_NOT_FOUND', "Sorry, this store is not available right now.");


/*
defined('REQUEST_KEYS_FAILURE') OR define('REQUEST_KEYS_FAILURE', 'Request object keys are missing.');
defined('INVALID_USER') OR define('INVALID_USER', 'Please enter valid User ID.');
defined('LOGIN_SUCCESS') OR define('LOGIN_SUCCESS', 'Logged Successfully.');
defined('LOGIN_FAILURE') OR define('LOGIN_FAILURE', 'Invalid Credentials.');
defined('RECORD_NOT_FOUND') OR define('RECORD_NOT_FOUND', 'User data is not available');
defined('ACCOUNT_BLOCKED') OR define('ACCOUNT_BLOCKED', 'Sorry your account has been locked out, please use <b>Forgot Your Password</b>.<br>');
defined('INCORRECT_PASSWORD') OR define('INCORRECT_PASSWORD', 'Please enter correct Password.');
defined('LOGOUT_SUCCESS') OR define('LOGOUT_SUCCESS', 'Logout successfully.');
defined('SIGNUP_SUCCESS') OR define('SIGNUP_SUCCESS', 'Registered successfully.');

defined('NEW_RECORD_SUCCESS') OR define('NEW_RECORD_SUCCESS', 'Added Successfully.');
defined('RECORD_UPDATE_SUCCESS') OR define('RECORD_UPDATE_SUCCESS', 'Updated Successfully.');
defined('DELETE_SUCCESS') OR define('DELETE_SUCCESS', 'Deleted Successfully.');
defined('SOMETHING_WENT_WORNG') OR define('SOMETHING_WENT_WORNG', 'Some thing went wrong. Please contact support.');
defined('MISSING_ACTION') OR define('MISSING_ACTION', 'Request action type is missing.');
defined('UNDEFINED_ACTION') OR define('UNDEFINED_ACTION', 'Unrecognized action type.');
defined('STORE_EXIST') OR define('STORE_EXIST', 'Store is already registered.');
defined('FLOOR_FAILURE') OR define('FLOOR_FAILURE', 'Floor is already registered.');
defined('ROOM_FAILURE') OR define('ROOM_FAILURE', 'Room is already registered.');
defined('EMAIL_FAILURE') OR define('EMAIL_FAILURE', 'Email is already registered.');
defined('NUMBER_FAILURE') OR define('NUMBER_FAILURE', 'Number is already registered.');
defined('PASSWORDS_DIFFERENT_FAILURE') OR define('PASSWORDS_DIFFERENT_FAILURE', 'Old password and New password must be different.');
defined('OLD_PASSWORD_FAILURE') OR define('OLD_PASSWORD_FAILURE', 'Old password not matched.');
defined('REQUEST_HEADERS_FAILURE') OR define('REQUEST_HEADERS_FAILURE', 'Request headers are missing.');
defined('SESSION_NOT_EXIST') OR define('SESSION_NOT_EXIST', 'Invalid session.');
defined('SESSION_FAILURE') OR define('SESSION_FAILURE', 'Session expired.');
defined('FILE_UPLOAD_FAIL') OR define('FILE_UPLOAD_FAIL', 'Upload file failed. please try again.');*/
