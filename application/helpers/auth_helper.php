<?php

if(!function_exists('_verify_session_token')) {
    function _verify_session_token( $headers) {
        $encp_key = explode("Bearer ", $headers['Authorization'])[1];
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = _session_key_enc_dec_ryption($encp_key, 'd');
            $decodedToken = AUTHORIZATION::validateTimestamp($token);
            if ($decodedToken != false) {
                return true;
            } else {
                return false;
            }
        }
    }
}

if(!function_exists('_decrypt_session_token')) {
    function _decrypt_session_token( $headers) {
        $encp_key = explode("Bearer ", $headers['Authorization'])[1];
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $token = _session_key_enc_dec_ryption($encp_key, 'd');
            $decodedToken = AUTHORIZATION::validateToken($token);
            return $decodedToken->id;
        }
    }
}


if(!function_exists('_verify_role')) {
    function _verify_role( $role_name, $role_id ) {
    	$code = 0;
    	$name = '';
        $CI =& get_instance();

        $CI->db->select('name');
        $found = $CI->db->get_where('roles', array('id' => $role_id, 'status' => 0))->row();
        if (count($found) > 0) {
            if ($found->name == $role_name) {
                return true;
            } else {
                return false;                
            }
        } else {
            return false;
        }
    }
}

if(!function_exists('_response_obj')) {
    function _response_obj( $code, $message, $data ) {
        $CI =& get_instance();
        $response = (object) array('status_code' => $code);

        if ($message) {
            $response->message = $message;
        }
        if ($data) {
            $response->data = $data;
        }
		return $CI->output
			 ->set_status_header($code)->set_content_type('application/json', 'utf-8')
        	 ->set_output(json_encode($response));
    }
}

if(!function_exists('_session_key_enc_dec_ryption')) {
    function _session_key_enc_dec_ryption($string, $action) {
        $secret_key = 'Cr@zyC0derSApp@$$$';
        $secret_iv = '$@$@7527';
        $output = false;
        $encrypt_method = "AES-256-CBC";
     
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;        
    }
}

if(!function_exists('_verify_record')) {
    function _verify_record($table_name, $col_name, $col_val) {
        $CI =& get_instance();
        
        // $found = $CI->db->get_where($table_name, array($col_name => $col_val, 'status' => 0));
        $record = $CI->db->get_where($table_name, array($col_name => $col_val, 'status' => 0))->num_rows();
        if ($record > 0) {
            return true;
        } else {
            return false;
        }
        
    }
}

if(!function_exists('_get_all_by_id')) {
    function _get_all_by_id($table_name, $col_name, $col_val) {
        $CI =& get_instance();
        
        $records = $CI->db->get_where($table_name, array($col_name => $col_val, 'status' => 0))->result();
        return $records;        
    }
}

if(!function_exists('_get_store__related_details')) {
    function _get_store__related_details($user_id) {
        $CI =& get_instance();
        
        $stmt = "select users.id as user_id, users.role_id, stores.id as store_id from users join stores on stores.status = users.status and users.id = stores.user_id where users.id = " . $user_id;

        $records = $CI->db->query($stmt)->row();
        return $records;        
    }
}


if(!function_exists('_get_role_details')) {
    function _get_role_details($req_value, $col_name, $col_val) {
        $CI =& get_instance();
        
        $CI->db->select($req_value);
        $found = $CI->db->get_where('roles', array($col_name => $col_val, 'status' => 0))->row();
        if (isset($found) && $found) {
            return $found;
        } else {
            return null;
        }
        
    }
}

if(!function_exists('_get_role_id')) {
    function _get_role_id($table_name, $id) {
        $CI =& get_instance();
        
        $CI->db->select('role_id');
        $found = $CI->db->get_where($table_name, array('id' => $id, 'status' => 0))->row();
        if (count($found) > 0) {
            return $found->role_id;
        } else {
            return 0;
        }
        
    }
}



