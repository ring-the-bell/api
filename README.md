# back-end

codeigniter + jwt (url -- https://github.com/ParitoshVaidya/CodeIgniter-JWT-Sample)

# functions

lcfirst() - converts the first character of a string to lowercase
ucwords() - converts the first character of each word in a string to uppercase
strtoupper() - converts a string to uppercase
strtolower() - converts a string to lowercase
ucfirst() - converts the first character of a string to uppercase.

#DB functions

$this->db->affected_rows();
$this->db->insert_id();

#Images path
Store Leavel
$img_path = explode("/home/c8ugqa8m8lxk/public_html/ringthebell/", $logo_data->full_path);
logo' => (isset($logo_data) && $logo_data->full_path) ? ("http://muscletechgym.com/ringthebell/" . $img_path[1]) : '',

CREATE TABLE `dd_b2c`.`deployment` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `version_no` VARCHAR(45) NOT NULL,
  `updates` TEXT NOT NULL,
  `deployeed_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`));

  CREATE TABLE `dd_b2c`.`suggestions` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `contact_no` VARCHAR(25) NOT NULL,
  `email` VARCHAR(45) NULL,
  `content` TEXT NOT NULL,
  PRIMARY KEY (`id`));

  ALTER TABLE `dd_b2c`.`suggestions` 
ADD COLUMN `user_id` BIGINT NOT NULL AFTER `id`,
ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP() AFTER `content`;



CREATE TABLE `audit_users` (
  `audit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `contact_no` varchar(45) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password_hash` text DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`audit_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TRIGGER after_users_insert
AFTER INSERT
ON users FOR EACH ROW
BEGIN        
  insert into audit_users (id,  role_id,  name,  contact_no,  address,  email,  password_hash,  country,  state,  city,
  status,  created_at,  updated_at) 
  values (NEW.id,  NEW.role_id,  NEW.name,  NEW.contact_no,  NEW.address,  NEW.email,  NEW.password_hash,  NEW.country,  NEW.state,  NEW.city,
  NEW.status,  NEW.created_at,  NEW.updated_at);    
END


CREATE TRIGGER before_users_update
BEFORE UPDATE
ON users FOR EACH ROW
BEGIN        
  insert into audit_users (id,  role_id,  name,  contact_no,  address,  email,  password_hash,  country,  state,  city,
  status,  created_at,  updated_at) 
  values (OLD.id,  OLD.role_id,  OLD.name,  OLD.contact_no,  OLD.address,  OLD.email,  OLD.password_hash,  OLD.country,  OLD.state,  OLD.city,
  OLD.status,  OLD.created_at,  OLD.updated_at);    
END


https://bootsnipp.com/tags/shop?page=8